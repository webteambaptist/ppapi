﻿using System;
using System.IO;

namespace PPAPI.Helpers
{
    public class PDFGenerator
    {
        public static Byte[] PdfSharpConvert(String html)
        {
            Byte[] res = null;
            using (MemoryStream ms = new MemoryStream())
            {
               
               var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(html, PdfSharp.PageSize.Legal);
               pdf.Save(ms);
               res = ms.ToArray();
            }
            return res;
        }
        public static byte[] generatePDF(string html)
        {
            var pdfBytes = new byte[0];
            try
            {
                var htmlContent = String.Format(html);
                var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
                var path = @"C:\Temp\"+DateTime.Now.Ticks.ToString()+".pdf";
                htmlToPdf.GeneratePdf(htmlContent, null, path);
                pdfBytes = File.ReadAllBytes(path);
            }
            catch (Exception e)
            {
                // Console.WriteLine(e.Message);
            }
            return pdfBytes;
        }
    }
}