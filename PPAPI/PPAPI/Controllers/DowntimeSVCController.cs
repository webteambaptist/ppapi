﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Configuration;
using System.Net.Http;
using System.IO;
using PPAPI.Models;
using PPAPI.DowntimePlansSVC;
using PPAPI.OnCallSVC;
using PPAPI.Bylaws2016SVC;
using System.Net.Http.Headers;
using PPAPI.Helpers;
using System.Security.Permissions;
using System.Runtime.InteropServices;
using System.Runtime.ConstrainedExecution;
using Microsoft.Win32.SafeHandles;
using System.Security;
using System.Text;
using System.Security.Principal;

namespace PPAPI.Controllers
{
    [RoutePrefix("api/files")]
    public class DowntimeSVCController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string DowntimePlansUrl = ConfigurationManager.AppSettings["DowntimePlans"].ToString();

        #region DowntimeAPI
        /// <summary>
        /// Get DowntimeFormPowerplansAdultItemFiles that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("DowntimeFormPowerplansAdultItemFiles")]
        public IHttpActionResult retrieveDowntimeFormPowerplansAdultFiles()
        {
            List<DowntimePlansSVC.DowntimeFormPowerplansAdultItem> docs = new List<DowntimeFormPowerplansAdultItem>();
            try
            {
                EMRZoneDataContext pp = new EMRZoneDataContext(new Uri(DowntimePlansUrl));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var a in pp.DowntimeFormPowerplansAdult)
                {
                    docs.Add(a);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/DowntimeFormPowerplansAdultItemFiles", ex);
                return BadRequest();
            }

            return Ok(docs);
        }

        /// <summary>
        /// Get DowntimeFormPowerplansAdultServiceLinesValueFiles that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("DowntimeFormPowerplansAdultServiceLinesValueFiles")]
        public IHttpActionResult retrieveDowntimeFormPowerplansAdultServiceLinesValueFiles()
        {
            List<DowntimePlansSVC.DowntimeFormPowerplansAdultServiceLinesValue> docs = new List<DowntimePlansSVC.DowntimeFormPowerplansAdultServiceLinesValue>();

            try
            {
                EMRZoneDataContext pp = new EMRZoneDataContext(new Uri(DowntimePlansUrl));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var a in pp.DowntimeFormPowerplansAdultServiceLines)
                {
                    docs.Add(a);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/DowntimeFormPowerplansAdultServiceLinesValueFiles", ex);
                return BadRequest();
            }

            return Ok(docs);
        }

        /// <summary>
        /// Get DowntimeFormPowerplansPedsItemFiles that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("DowntimeFormPowerplansPedsItemFiles")]
        public IHttpActionResult retrieveDowntimeFormPowerplansPedsItemFiles()
        {
            List<DowntimePlansSVC.DowntimeFormPowerplansPedsItem> docs = new List<DowntimePlansSVC.DowntimeFormPowerplansPedsItem>();

            try
            {               
                EMRZoneDataContext pp = new EMRZoneDataContext(new Uri(DowntimePlansUrl));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var a in pp.DowntimeFormPowerplansPeds)
                {
                    docs.Add(a);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/DowntimeFormPowerplansPedsItemFiles", ex);
                return BadRequest();
            }

            return Ok(docs);
        }

        /// <summary>
        /// Get DowntimeFormPowerplansPedsServiceLineValueFiles that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("DowntimeFormPowerplansPedsServiceLineValueFiles")]
        public IHttpActionResult retrieveDowntimeFormPowerplansPedsServiceLineValueFiles()
        {
            List<DowntimePlansSVC.DowntimeFormPowerplansPedsServiceLineValue> docs = new List<DowntimePlansSVC.DowntimeFormPowerplansPedsServiceLineValue>();

            try
            {
                EMRZoneDataContext pp = new EMRZoneDataContext(new Uri(DowntimePlansUrl));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var a in pp.DowntimeFormPowerplansPedsServiceLine)
                {
                    docs.Add(a);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/DowntimeFormPowerplansPedsServiceLineValueFiles", ex);
                return BadRequest();
            }

            return Ok(docs);
        }

        /// <summary>
        /// Get DowntimeFormsPowerPlansAdultsItemFiles that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("DowntimeFormsPowerPlansAdultsItemFiles")]
        public IHttpActionResult retrieveDowntimeFormsPowerPlansAdultsItemFiles()
        {
            List<DowntimePlansSVC.DowntimeFormsPowerPlansAdultsItem> docs = new List<DowntimePlansSVC.DowntimeFormsPowerPlansAdultsItem>();

            try
            {
                EMRZoneDataContext pp = new EMRZoneDataContext(new Uri(DowntimePlansUrl));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var a in pp.DowntimeFormsPowerPlansAdults)
                {
                    docs.Add(a);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/DowntimeFormsPowerPlansAdultsItemFiles", ex);
                return BadRequest();
            }

            return Ok(docs);
        }

        /// <summary>
        /// Post DowntimeFormsPowerPlansAdultsItemFiles PDF based on the ID
        /// </summary>
        [HttpPost]
        [Route("DowntimeFormsPowerPlansAdultsItemFiles/{id}")]
        public IHttpActionResult retrieveDowntimeFormsPowerPlansAdultsItemFilesPDFByID(int id)
        {
            try
            {
                var headers = Request.Headers;
                int linkId = id;

                EMRZoneDataContext pp = new EMRZoneDataContext(new Uri(DowntimePlansUrl));
                pp.Credentials = CredentialCache.DefaultCredentials;

                List<DowntimePlansSVC.DowntimeFormsPowerPlansAdultsItem> documentList = new List<DowntimeFormsPowerPlansAdultsItem>();
                documentList = pp.DowntimeFormsPowerPlansAdults.ToList();
                var doc = (from x in documentList where x.Id == linkId select x).First();

                System.Data.Services.Client.DataServiceStreamResponse DataServiceStreamResponse = pp.GetReadStream(doc);
                Stream stream = DataServiceStreamResponse.Stream;

                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(stream)
                };

                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = doc.Name
                };

                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

                var response = ResponseMessage(result);

                return response;
            }
            catch (Exception ex)
            {
                log.Info("api/files/DowntimeFormsPowerPlansAdultsItemFiles/{id}", ex);
                return BadRequest();
            }
        }

        /// <summary>
        /// Get DowntimeFormsPowerPlansAdultsServiceLinesValueFiles that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("DowntimeFormsPowerPlansAdultsServiceLinesValueFiles")]
        public IHttpActionResult retrieveDowntimeFormsPowerPlansAdultsServiceLinesValueFiles()
        {
            List<DowntimePlansSVC.DowntimeFormsPowerPlansAdultsServiceLinesValue> docs = new List<DowntimePlansSVC.DowntimeFormsPowerPlansAdultsServiceLinesValue>();

            try
            {               
                EMRZoneDataContext pp = new EMRZoneDataContext(new Uri(DowntimePlansUrl));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var a in pp.DowntimeFormsPowerPlansAdultsServiceLines)
                {
                    docs.Add(a);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/DowntimeFormsPowerPlansAdultsServiceLinesValueFiles", ex);
                return BadRequest();
            }

            return Ok(docs);
        }

        /// <summary>
        /// Get DowntimeFormsPowerPlansPedsItemFiles that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("DowntimeFormsPowerPlansPedsItemFiles")]
        public IHttpActionResult retrieveDowntimeFormsPowerPlansPedsItemFiles()
        {
            List<DowntimePlansSVC.DowntimeFormsPowerPlansPedsItem> docs = new List<DowntimePlansSVC.DowntimeFormsPowerPlansPedsItem>();

            try
            {
                EMRZoneDataContext pp = new EMRZoneDataContext(new Uri(DowntimePlansUrl));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var a in pp.DowntimeFormsPowerPlansPeds)
                {
                    docs.Add(a);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/DowntimeFormsPowerPlansPedsItemFiles", ex);
                return BadRequest();
            }

            return Ok(docs);
        }

        /// <summary>
        /// Post DowntimeFormsPowerPlansPedsItemFiles PDF based on the ID
        /// </summary>
        [HttpPost]
        [Route("DowntimeFormsPowerPlansPedsItemFiles/{id}")]
        public IHttpActionResult retrieveDowntimeFormsPowerPlansPedsItemFilesPDFByID(int id)
        {
            try
            {
                var headers = Request.Headers;
                int linkId = id;

                EMRZoneDataContext pp = new EMRZoneDataContext(new Uri(DowntimePlansUrl));
                pp.Credentials = CredentialCache.DefaultCredentials;

                List<DowntimePlansSVC.DowntimeFormsPowerPlansPedsItem> documentList = new List<DowntimeFormsPowerPlansPedsItem>();
                documentList = pp.DowntimeFormsPowerPlansPeds.ToList();
                var doc = (from x in documentList where x.Id == linkId select x).First();

                System.Data.Services.Client.DataServiceStreamResponse DataServiceStreamResponse = pp.GetReadStream(doc);
                Stream stream = DataServiceStreamResponse.Stream;

                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(stream)
                };

                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = doc.Name
                };

                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

                var response = ResponseMessage(result);

                return response;
            }
            catch(Exception ex)
            {
                log.Info("api/files/DowntimeFormsPowerPlansPedsItemFiles/{id}", ex);
                return BadRequest();            
            }
        }

        /// <summary>
        /// Get DowntimeFormsPowerPlansPedsServiceLineValueFiles that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("DowntimeFormsPowerPlansPedsServiceLineValueFiles")]
        public IHttpActionResult retrieveDowntimeFormsPowerPlansPedsServiceLineValueFiles()
        {
            List<DowntimePlansSVC.DowntimeFormsPowerPlansPedsServiceLineValue> docs = new List<DowntimePlansSVC.DowntimeFormsPowerPlansPedsServiceLineValue>();

            try
            {
                EMRZoneDataContext pp = new EMRZoneDataContext(new Uri(DowntimePlansUrl));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var a in pp.DowntimeFormsPowerPlansPedsServiceLine)
                {
                    docs.Add(a);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/DowntimeFormsPowerPlansPedsServiceLineValueFiles", ex);
                return BadRequest();
            }

            return Ok(docs);
        }
        #endregion
    }
}