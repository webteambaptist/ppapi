﻿using PPAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace PPAPI.Controllers
{
    /// <summary>
    /// CRUD API for Social feed
    /// </summary>
    
    [RoutePrefix("api/feeds")]
    public class SocialController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// delete sub for given physician ID
        /// </summary>
        /// <returns></returns>
        [Route("delSub")]
        [HttpGet]
        public IHttpActionResult delSubs()
        {
            var headers = Request.Headers;
            string phyid = ""; //subscription physician ID
            string subid = ""; //subscriber to delete

            try
            {
                var ppEntities = new PhysiciansPortalEntities();


                if (headers.Contains("phyid"))
                {
                    phyid = headers.GetValues("phyid").First();
                }

                if (headers.Contains("subid"))
                {
                    subid = headers.GetValues("subid").First();
                }


                if ((phyid == null) || (subid == null))
                {
                    return Json("Missing Data");
                }
                else
                {
                    ppEntities.delMatrix(phyid, subid);
                    ppEntities.SaveChanges();

                    return Ok();
                }
            }
            catch (Exception ex)
            {
                log.Info("api/feeds/delSub", ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// delete post
        /// </summary>
        /// <returns></returns>
        [Route("delPost")]
        [HttpGet]
        public IHttpActionResult delPost()
        {
            var headers = Request.Headers;
            string postID = "";

            try
            {
                if (headers.Contains("postid"))
                {
                    postID = headers.GetValues("postid").First();
                }
                else
                {
                    return Json("Missing Data");
                }

                if (postID == null)
                {
                    return Json("Missing Data");
                }
                else
                {
                    var ppEntities = new PhysiciansPortalEntities();
                    ppEntities.delPost(int.Parse(postID));
                    return Ok();
                }

            }
            catch (Exception ex)
            {
                log.Info("api/feeds/delPost", ex);
                return BadRequest();
            }
        }

        /// <summary>
        /// get subscribers for a given feed
        /// </summary>
        /// <returns></returns>
        [Route("selSocial")]
        [HttpGet]
        public IHttpActionResult selSubscribes()
        {
            var headers = Request.Headers;
            string profileEID = ""; //subscription physician ID
            string loggedEchoID = "";
            SubsPubs subPub = new SubsPubs();
            subPub.Subscribers = new List<Subs>();
            subPub.Subscriptions = new List<Subs>();

            try
            {
                var ppEntities = new PhysiciansPortalEntities();
                var matrix = ppEntities.SocialFeedMatrices;
                var people = ppEntities.GeneralPhysicianInfoes;

                if (headers.Contains("profileEID"))
                {
                    profileEID = headers.GetValues("profileEID").First();
                }

                if (profileEID == null)
                {
                    return Json("Missing Data");
                }

                if (headers.Contains("loggedEchoID"))
                {
                    loggedEchoID = headers.GetValues("loggedEchoID").First();
                }

                if (loggedEchoID == null)
                {
                    return Json("Missing Data");
                }
                else
                {
                    try
                    {

                        var subList = matrix.Where(x => x.SubscriptionPhysicianID == profileEID && x.SubscribersPhysicianID == loggedEchoID).First();

                        if (subList == null)
                        {
                            subPub.Subscribed = false;
                        }
                        else
                        {
                            subPub.Subscribed = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Info("api/feeds/selSocial", ex);
                        subPub.Subscribed = false;
                    }

                   

                    var subscribers = matrix.Where(x => x.SubscriptionPhysicianID == profileEID).ToList();

                    foreach (var s in subscribers)
                    {
                        try
                        {

                            Subs u = new Subs();
                            u.ID = s.ID;
                            u.SubscriptionPhysicianID = s.SubscriptionPhysicianID;
                            u.SubscribersPhysicianID = s.SubscribersPhysicianID;
                            u.SubscribersPhysicianName = getName(s.SubscribersPhysicianID);
                            u.SubscriptionPhysicianName = getName(s.SubscriptionPhysicianID);
                            subPub.Subscribers.Add(u);
                        }
                        catch (Exception ex)
                        {
                            log.Info("api/feeds/selSocial", ex);
                            continue;
                        }
                    }

                    var subscriptions = matrix.Where(x => x.SubscribersPhysicianID == profileEID).ToList();

                    foreach (var s in subscriptions)
                    {
                        try
                        { 
                            Subs u = new Subs();
                            u.ID = s.ID;
                            u.SubscriptionPhysicianID = s.SubscriptionPhysicianID;
                            u.SubscribersPhysicianID = s.SubscribersPhysicianID;
                            u.SubscribersPhysicianName = getName(s.SubscribersPhysicianID);
                            u.SubscriptionPhysicianName = getName(s.SubscriptionPhysicianID);
                            subPub.Subscriptions.Add(u);
                        }
                        catch (Exception ex)
                        {
                            log.Info("api/feeds/selSocial", ex);
                            continue;
                        }
                    }

                    return Json(subPub);
                }

            }
            catch (Exception ex)
            {
                log.Info("api/feeds/selSocial", ex);
                return BadRequest();
            }
        }

        private string getName(string subscribersPhysicianID)
        {
            var ppEntities = new PhysiciansPortalEntities();
            var people = ppEntities.GeneralPhysicianInfoes;

            string name;

            try
            {
                var person = people.Where(x => x.ECHOID == subscribersPhysicianID).First();

                if (person == null)
                {
                    return "";
                }
                else
                {
                    name = person.FirstName + " " + person.LastName + ", " + person.Credentials;
                    return name;
                }

            }
            catch(Exception ex)
            {
                log.Info("SocialController/getName", ex);
                return "";
            }


        }

        /// <summary>
        /// get subscribers for a given feed
        /// </summary>
        /// <returns></returns>
        [Route("selSubs")]
        [HttpGet]
        public IHttpActionResult selSubs()
        {
            var headers = Request.Headers;
            string phyid = ""; //subscription physician ID

            try
            {
                var ppEntities = new PhysiciansPortalEntities();
                var matrix = ppEntities.SocialFeedMatrices;

                if (headers.Contains("phyid"))
                {
                    phyid = headers.GetValues("phyid").First();
                }

                if (phyid == null)
                {
                    return Json("");
                }
                else
                {
                    var subList = matrix.Where(x => x.SubscriptionPhysicianID == phyid).First();

                    return Json(subList);
                }

            }
            catch (Exception ex)
            {
                log.Info("api/feeds/selSubs", ex);
                return BadRequest();
            }
        }

        /// <summary>
        /// get subscribers for a given feed
        /// </summary>
        /// <returns></returns>
        [Route("insSubs")]
        [HttpPost]
        public IHttpActionResult insSubs([FromBody] SocialFeedMatrix sfm)
        {
            var headers = Request.Headers;

            try
            {
                var ppEntities = new PhysiciansPortalEntities();
                // var matrix = ppEntities.SocialFeedMatrices;
                ppEntities.insSubs(sfm.SubscriptionPhysicianID, sfm.SubscribersPhysicianID);
               // matrix.Add(sfm);
                ppEntities.SaveChanges();

                return Ok();

            }
            catch (Exception ex)
            {
                log.Info("api/feeds/insSubs", ex);
                return BadRequest();
            }
        }

        /// <summary>
        /// get social posts with attachments
        /// </summary>
        /// <returns></returns>
        [Route("getfeeds")]
        [HttpGet]
        public IHttpActionResult selFeeds()
        {
            var headers = Request.Headers;
            string echoID = "";
            
            try
            {
                var ppEntities = new PhysiciansPortalEntities();
                var feeds =  ppEntities.SocialFeedMains;

                if (headers.Contains("echoid"))
                {
                    echoID = headers.GetValues("echoid").First();
                }

                if (feeds == null)
                {
                    return Json("");
                }
                else
                {
                    List<SocialData> sds = new List<SocialData>();
                    
                    var sdfeeds = feeds.Where(x => x.ECHOID == echoID).ToList();

                    var sdfeedsorder = sdfeeds.OrderByDescending(x => x.PostDate);

                    foreach (var s in sdfeedsorder)
                    {
                        SocialData sd = new SocialData();

                        sd.ID = s.ID;
                        sd.PostID = s.PostID;
                        sd.PostAuthor = s.PostAuthor;
                        sd.PostDate = s.PostDate.ToShortDateString();
                        sd.PostBody = s.PostBody;
                        sd.CreatedDT = s.CreatedDT.ToShortDateString();
                        sd.LastUpdateDT = s.LastUpdateDT.ToShortDateString();
                        sd.ECHOID = s.ECHOID;
                        sd.PostAuthorEchoID = s.PostAuthorEchoID;
                        sd.attachments = new List<SocialFeedAttachment>();
                        //get the attachments
                        var files = ppEntities.selSocialFeed2(s.PostID);

                        foreach (var f in files)
                        {
                            try
                            {
                                SocialFeedAttachment sfa = new SocialFeedAttachment();
                                sfa.CreateDT = f.CreateDT;
                                sfa.ID = f.ID;
                             // sfa.PostAttachment = f.PostAttachment;
                                sfa.PostID = f.PostID;
                                sfa.PostFileName = f.PostFileName;

                                sd.attachments.Add(sfa);
                            }
                            catch (Exception ex)
                            {
                                log.Info("api/feeds/getfeeds", ex);
                                continue;
                            }
                        }

                        sds.Add(sd);
                    }
                    return Json(sds);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/feeds/getfeeds", ex);
                return BadRequest();
            }
        }

        /// <summary>
        /// get file attachment for given ID
        /// </summary>
        /// <returns></returns>
        [Route("getFile")]
        [HttpGet]
        public IHttpActionResult getFile()
        {
            var headers = Request.Headers;
            var ppEntities = new PhysiciansPortalEntities();
            int ID = 0;

            try
            {
                if (headers.Contains("ID"))
                {
                    string link = headers.GetValues("ID").First();

                    try
                    {
                        ID = int.Parse(link);
                    }
                    catch (Exception ex)
                    {
                        log.Info("api/feeds/getFile", ex);
                        return BadRequest();
                    }
                }

                var doc = ppEntities.SocialFeedAttachments.Where(x => x.ID == ID).First();

                MemoryStream stream = new MemoryStream(doc.PostAttachment);

                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(stream)
                };

                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = doc.PostFileName
                };
                /*Checks the extenstion then correctly applys MediaTypeHeaderValue to download the file correctly (This prevents corrupted files)*/
                if (doc.PostFileName.Contains(".docx"))
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                }
                else if (doc.PostFileName.Contains(".doc"))
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/msword");
                }
                else if (doc.PostFileName.Contains(".pdf"))
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                }
                else if (doc.PostFileName.Contains(".xlsx"))
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                }
                else if (doc.PostFileName.Contains(".xls"))
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                }
                else
                {
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                }
                                
                var response = ResponseMessage(result);

                return response;
            }
            catch (Exception ex)
            {
                log.Info("api/feeds/getFile", ex);
                return BadRequest();
            }
        }

        /// <summary>
        /// save social posts with attachments
        /// </summary>
        /// <param name="sd"></param>
        /// <returns></returns>
        [Route("savefeeds")]
        [HttpPost]
        public HttpResponseMessage insFeeds()
        {            
            var httpRequest = HttpContext.Current.Request; //
            var headers = Request.Headers;

            string postAuthor = "";
            string postBody = "";
            string echoID = "";
            string postAuthorEchoID = "";

            if (headers.Contains("echoid"))
            {
                echoID = headers.GetValues("echoid").First();
            }

            if (headers.Contains("postBody"))
            {
               postBody = headers.GetValues("postBody").First();
            }

            if (headers.Contains("postAuthor"))
            {
                postAuthor = headers.GetValues("postAuthor").First();
            }

            if (headers.Contains("PostAuthorid"))
            {
                postAuthorEchoID = headers.GetValues("PostAuthorid").First();
            }

            //process text side first
            var ppEntities = new PhysiciansPortalEntities();

            int postID = 0;

            try
            {
                SocialFeedMain sd = new SocialFeedMain();
                sd.ID = 0;
                    //sd.PostID = s.PostID;
                    sd.PostAuthor = postAuthor;
                    sd.PostDate = DateTime.Now;
                    sd.PostBody = postBody;
                    sd.CreatedDT = DateTime.Now;
                    sd.LastUpdateDT = DateTime.Now;
                    sd.ECHOID = echoID;
                    sd.PostAuthorEchoID = postAuthorEchoID;
                    ppEntities.SocialFeedMains.Add(sd);
                    ppEntities.SaveChanges();

                    //get last post created by this ECHOID to get related POSTID
                    var feeds = ppEntities.SocialFeedMains;
                    var posts = feeds.Where(x => x.ECHOID == sd.ECHOID).ToList();
                    postID = posts.Last().PostID;

                if (httpRequest.Files.Count > 0)
                {
                    for (int i = 0; i < (httpRequest.Files.Count); i++)
                    {
                        SocialFeedAttachment sfa = new SocialFeedAttachment();
                        sfa.PostID = postID;
                        sfa.CreateDT = DateTime.Now;
                        var file = httpRequest.Files[i];

                        using (var binaryReader = new BinaryReader(httpRequest.Files[i].InputStream))
                        {                           
                            sfa.PostAttachment = binaryReader.ReadBytes(httpRequest.Files[i].ContentLength);
                            var pos = httpRequest.Files[i].FileName.LastIndexOf("\\") + 1;
                            sfa.PostFileName = httpRequest.Files[i].FileName.Substring(pos, httpRequest.Files[i].FileName.Length - pos);
                        }

                        ppEntities.SocialFeedAttachments.Add(sfa);
                    }

                    ppEntities.SaveChanges();
                }
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                log.Info("api/feeds/savefeeds", ex);
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }

        }

        /// <summary>
        /// update entire post, deletes original attachments and reload them
        /// </summary>
        /// <param name="sd"></param>
        /// <returns></returns>
        [Route("updfeedall")]
        [HttpPost]
        public HttpResponseMessage updFeeds()
        {
            var httpRequest = HttpContext.Current.Request; //
            var headers = Request.Headers;
            var ppEntities = new PhysiciansPortalEntities();

            string postAuthor = "";
            string postBody = "";
            string echoID = "";
            string postAuthorEchoID = "";
            string postID = "";

            if (headers.Contains("echoid"))
            {
                echoID = headers.GetValues("echoid").First();
            }

            if (headers.Contains("postBody"))
            {
                postBody = headers.GetValues("postBody").First();
            }

            if (headers.Contains("postAuthor"))
            {
                postAuthor = headers.GetValues("postAuthor").First();
            }

            if (headers.Contains("PostAuthorid"))
            {
                postAuthorEchoID = headers.GetValues("PostAuthorid").First();
            }

            if (headers.Contains("postid"))
            {
                postID = headers.GetValues("postid").First();
            }

            try
            {
                int iPostID = int.Parse(postID); 

                var feeds = ppEntities.SocialFeedMains;
                var post = feeds.Where(x => x.PostID == iPostID).FirstOrDefault();
                post.PostBody = postBody;
                ppEntities.SaveChanges();
                

                if (httpRequest.Files.Count > 0)
                {
                    //delete attachments
                    ppEntities.delAttach(iPostID);
                    ppEntities.SaveChanges();

                    foreach (string f in httpRequest.Files)
                    {
                        SocialFeedAttachment sfa = new SocialFeedAttachment();
                        sfa.PostID = iPostID;
                        var file = httpRequest.Files[f];
                        sfa.CreateDT = DateTime.Now;

                        using (var binaryReader = new BinaryReader(httpRequest.Files[f].InputStream))
                        {
                            sfa.PostAttachment = binaryReader.ReadBytes(httpRequest.Files[f].ContentLength);
                            sfa.PostFileName = httpRequest.Files[f].FileName;
                        }

                        ppEntities.SocialFeedAttachments.Add(sfa);
                    }

                    ppEntities.SaveChanges();
                }
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                log.Info("api/feeds/updfeedall", ex);
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }

        }

        ///// <summary>
        ///// update only the body and title of the post
        ///// </summary>
        ///// <param name="sd"></param>
        ///// <returns></returns>
        //[Route("updfeedonly")]
        //[HttpPost]
        //public HttpResponseMessage updFeedsOnly()
        //{
        //    var httpRequest = HttpContext.Current.Request; //
        //    var headers = Request.Headers;
        //    var ppEntities = new PhysiciansPortalEntities();

        //    string postAuthor = "";
        //    string postBody = "";
        //    string echoID = "";
        //    string postAuthorEchoID = "";
        //    string postID = "";

        //    if (headers.Contains("echoid"))
        //    {
        //        echoID = headers.GetValues("echoid").First();
        //    }

        //    if (headers.Contains("postBody"))
        //    {
        //        postBody = headers.GetValues("postBody").First();
        //    }

        //    if (headers.Contains("postAuthor"))
        //    {
        //        postAuthor = headers.GetValues("postAuthor").First();
        //    }

        //    if (headers.Contains("PostAuthorid"))
        //    {
        //        postAuthorEchoID = headers.GetValues("PostAuthorid").First();
        //    }

        //    if (headers.Contains("postid"))
        //    {
        //        postID = headers.GetValues("postid").First();
        //    }

        //    try
        //    {
        //        if (sd != null)
        //        {
        //            var feeds = ppEntities.SocialFeedMains;
        //            var post = feeds.Where(x => x.PostID == sd.PostID).FirstOrDefault();
        //            post.PostBody = sd.PostBody;
        //            ppEntities.SaveChanges();
        //        }

        //        return Request.CreateResponse(HttpStatusCode.OK);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
        //    }

        //}

        ///// <summary>
        ///// delete all attachments and reinsert for a given post
        ///// </summary>
        ///// <param name="sd"></param>
        ///// <returns></returns>
        //[Route("updattachonly")]
        //[HttpPost]
        //public HttpResponseMessage updAttachOnly([FromBody] SocialData sd)
        //{
        //    var httpRequest = HttpContext.Current.Request; //

        //    //process text side first
        //    var ppEntities = new PhysiciansPortalEntities();

        //    int postID = 0;

        //    try
        //    {
        //        if (httpRequest.Files.Count > 0)
        //        {
        //            //delete attachments
        //            ppEntities.delAttach(sd.PostID);
        //            ppEntities.SaveChanges();

        //            foreach (string f in httpRequest.Files)
        //            {
        //                SocialFeedAttachment sfa = new SocialFeedAttachment();
        //                sfa.PostID = postID;
        //                var file = httpRequest.Files[f];
        //                sfa.CreateDT = DateTime.Now;

        //                using (var binaryReader = new BinaryReader(httpRequest.Files[f].InputStream))
        //                {
        //                    sfa.PostAttachment = binaryReader.ReadBytes(httpRequest.Files[f].ContentLength);
        //                    sfa.PostFileName = httpRequest.Files[f].FileName;
        //                }

        //                ppEntities.SocialFeedAttachments.Add(sfa);
        //            }

        //            ppEntities.SaveChanges();
        //        }
        //        return Request.CreateResponse(HttpStatusCode.OK);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
        //    }

        //}
    }
}
