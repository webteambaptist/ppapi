﻿using PPAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PPAPI.Controllers
{
    /// <summary>
    /// API for System Alerts
    /// </summary>
    [RoutePrefix("api/alerts")]
    public class AlertsController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// returns all enabled alerts in SmAlert format
        /// </summary>
        /// <returns></returns>
        [Route("getAlerts")]
        [HttpGet]
        public IHttpActionResult selAlerts()
        {

            try
            {
                var SAE = new SystemAlertsEntities();

                var alerts = SAE.SystemAlerts.Where(x => x.IsEnabled == true && x.EndDt > DateTime.Now);

                List<SystemAlertsSm> smalerts = new List<SystemAlertsSm>();

                foreach (var a in alerts)
                {
                    SystemAlertsSm smalert = new SystemAlertsSm();
                    smalert.id = a.id;
                    smalert.Message = a.Message;
                    smalert.IsPhysicianPortalVisible = a.IsPhysicianPortalVisible;
                    smalert.StartDt = a.StartDt;
                    smalert.EndDt = a.EndDt;
                    smalert.IsEverywhere = a.IsEverywhere;
                   // smalert.IsEnabled = a.IsEnabled;
                    smalerts.Add(smalert);

                }

                try
                {
                    if (ConfigurationManager.AppSettings["ISDEBUG"] != "0")
                    {
                        if (smalerts.Count < 1)
                        {
                            SystemAlertsSm smalert = new SystemAlertsSm();
                            smalert.id = 9;
                            smalert.Message = "TEST Squirrels in the breakroom TEST";
                            smalert.IsPhysicianPortalVisible = true;
                            smalert.StartDt = DateTime.Now.AddHours(-1);
                            smalert.EndDt = DateTime.Now.AddHours(3);
                            smalert.IsEverywhere = false;
                            // smalert.IsEnabled = a.IsEnabled;
                            smalerts.Add(smalert);
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Info("api/alerts/getAlerts", ex);                       
                }

                return Json(smalerts);
            }
            catch (Exception ex)
            {
                log.Info("api/alerts/getAlerts", ex);
                return BadRequest(ex.Message);
            }
        }
    }
}
