﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Configuration;
using System.Net.Http;
using System.IO;
using PPAPI.Models;
using PPAPI.DowntimePlansSVC;
using PPAPI.OnCallSVC;
using PPAPI.Bylaws2016SVC;
using System.Net.Http.Headers;
using PPAPI.Helpers;
using System.Security.Permissions;
using System.Runtime.InteropServices;
using System.Runtime.ConstrainedExecution;
using Microsoft.Win32.SafeHandles;
using System.Security;
using System.Text;
using System.Security.Principal;

namespace PPAPI.Controllers
{
    [RoutePrefix("api/files")]
    public class Downtime2016SVCController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string DowntimePlans2016Url = ConfigurationManager.AppSettings["DowntimePlans2016"].ToString();

        #region Downtime2016API
        /// <summary>
        /// Get DowntimeForms2016PowerPlansAdults that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("DowntimeForms2016PowerPlansAdults")]
        public IHttpActionResult retrieveDowntimeForms2016PowerPlansAdults()
        {
            List<DowntimePlans2016SVC.DowntimeFormsPowerPlansAdultsItem> docs = new List<DowntimePlans2016SVC.DowntimeFormsPowerPlansAdultsItem>();
            PowerPlansAdults powerPlansAdults = new PowerPlansAdults();
            powerPlansAdults.PowerPlansAdultsItem = new List<PowerPlanItems>();
            try
            {
                DowntimePlans2016SVC.InformaticsDataContext pp = new DowntimePlans2016SVC.InformaticsDataContext(new Uri(DowntimePlans2016Url));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var item in pp.DowntimeFormsPowerPlansAdults)
                {
                   
                    PowerPlanItems powerPlansItems = new PowerPlanItems();
                    powerPlansItems.ID = item.Id;
                    powerPlansItems.Name = item.Name.Replace(".pdf", "");
                    powerPlansItems.ServiceLineValue = item.ServiceLineValue;
                    powerPlansAdults.PowerPlansAdultsItem.Add(powerPlansItems);
                }
                
            }
            catch (Exception ex)
            {
                log.Info("api/files/DowntimeForms2016PowerPlansAdults", ex);
                return BadRequest();
            }

            return Ok(powerPlansAdults);
        }

        /// <summary>
        /// Get DowntimeForm2016PowerplansAdultServiceLinesValues that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("DowntimeForm2016PowerPlansAdultServiceLinesValues")]
        public IHttpActionResult retrieveDowntimeForm2016PowerPlansAdultServiceLinesValues()
        {
            List<DowntimePlans2016SVC.DowntimeFormsPowerPlansAdultsServiceLineValue> docs = new List<DowntimePlans2016SVC.DowntimeFormsPowerPlansAdultsServiceLineValue>();

            try
            {
                DowntimePlans2016SVC.InformaticsDataContext pp = new DowntimePlans2016SVC.InformaticsDataContext(new Uri(DowntimePlans2016Url));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var a in pp.DowntimeFormsPowerPlansAdultsServiceLine)
                {
                    docs.Add(a);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/DowntimeForm2016PowerPlansAdultServiceLinesValues", ex);
                return BadRequest();
            }

            return Ok(docs);
        }

        /// <summary>
        /// Get DowntimeForm2016PowerplansPeds that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("DowntimeForms2016PowerPlansPeds")]
        public IHttpActionResult retrieveDowntimeForm2016PowerPlansPeds()
        {
            List<DowntimePlans2016SVC.DowntimeFormsPowerPlansPedsItem> docs = new List<DowntimePlans2016SVC.DowntimeFormsPowerPlansPedsItem>();
            PowerPlansPeds powerPlansPeds = new PowerPlansPeds();
            powerPlansPeds.PowerPlansPedsItem = new List<PowerPlanItems>();         
            try
            {
                DowntimePlans2016SVC.InformaticsDataContext pp = new DowntimePlans2016SVC.InformaticsDataContext(new Uri(DowntimePlans2016Url));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var item in pp.DowntimeFormsPowerPlansPeds)
                {
                        PowerPlanItems powerPlansItems = new PowerPlanItems();
                        powerPlansItems.ID = item.Id;
                        powerPlansItems.Name = item.Name.Replace(".pdf", "");
                        powerPlansItems.ServiceLineValue = item.ServiceLineValue;
                        powerPlansPeds.PowerPlansPedsItem.Add(powerPlansItems);                    
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/DowntimeForms2016PowerPlansPeds", ex);
                return BadRequest();
            }

            return Ok(powerPlansPeds);
        }

        /// <summary>
        /// Get DowntimeForm2016PowerplansPedsServiceLineValues that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("DowntimeForm2016PowerPlansPedsServiceLineValues")]
        public IHttpActionResult retrieveDowntimeForm2016PowerPlansPedsServiceLineValues()
        {
            List<DowntimePlans2016SVC.DowntimeFormsPowerPlansPedsServiceLineValue> docs = new List<DowntimePlans2016SVC.DowntimeFormsPowerPlansPedsServiceLineValue>();

            try
            {
                DowntimePlans2016SVC.InformaticsDataContext pp = new DowntimePlans2016SVC.InformaticsDataContext(new Uri(DowntimePlans2016Url));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var a in pp.DowntimeFormsPowerPlansPedsServiceLine)
                {
                    docs.Add(a);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/DowntimeForm2016PowerPlansPedsServiceLineValues", ex);
                return BadRequest();
            }

            return Ok(docs);
        }

        /// <summary>
        /// Post DowntimeForms2016PowerPlansAdults PDF based on the ID through Headers
        /// </summary>
        [HttpPost]
        [Route("DowntimeForms2016PowerPlansAdultsByID")]
        public IHttpActionResult retrieveDowntimeForms2016PowerPlansAdultsPDFByID()
        {
            try
            {
                var headers = Request.Headers;
                string linkId;
                               
                DowntimePlans2016SVC.DowntimeFormsPowerPlansAdultsItem doc = new DowntimePlans2016SVC.DowntimeFormsPowerPlansAdultsItem();

                DowntimePlans2016SVC.InformaticsDataContext pp = new DowntimePlans2016SVC.InformaticsDataContext(new Uri(DowntimePlans2016Url));
                pp.Credentials = CredentialCache.DefaultCredentials;

                List<DowntimePlans2016SVC.DowntimeFormsPowerPlansAdultsItem> documentList = new List<DowntimePlans2016SVC.DowntimeFormsPowerPlansAdultsItem>();
                documentList = pp.DowntimeFormsPowerPlansAdults.ToList();

                if (headers.Contains("ID"))
                {
                    linkId = headers.GetValues("ID").First();
                    doc = (from x in documentList where x.Id.ToString() == linkId select x).First();
                }
                else
                {
                    log.Info("api/files/DowntimeForms2016PowerPlansAdultsPDFByID-HEADER DID NOT INCLUDE (ID) ");
                    return BadRequest();
                }

                System.Data.Services.Client.DataServiceStreamResponse DataServiceStreamResponse = pp.GetReadStream(doc);
                Stream stream = DataServiceStreamResponse.Stream;

                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(stream)
                };

                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = doc.Name
                };

                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

                var response = ResponseMessage(result);

                return response;
            }
            catch (Exception ex)
            {
                log.Info("api/files/DowntimeForms2016PowerPlansAdults/POST", ex);
                return BadRequest();
            }
        }

        /// <summary>
        /// Post DowntimeForms2016PowerPlansPeds PDF based on the ID
        /// </summary>
        [HttpPost]
        [Route("DowntimeForms2016PowerPlansPedsByID")]
        public IHttpActionResult retrieveDowntimeForms2016PowerPlansPedsPDFByID()
        {
            try
            {
                var headers = Request.Headers;
                string linkId;

                DowntimePlans2016SVC.DowntimeFormsPowerPlansPedsItem doc = new DowntimePlans2016SVC.DowntimeFormsPowerPlansPedsItem();

                DowntimePlans2016SVC.InformaticsDataContext pp = new DowntimePlans2016SVC.InformaticsDataContext(new Uri(DowntimePlans2016Url));
                pp.Credentials = CredentialCache.DefaultCredentials;

                List<DowntimePlans2016SVC.DowntimeFormsPowerPlansPedsItem> documentList = new List<DowntimePlans2016SVC.DowntimeFormsPowerPlansPedsItem>();
                documentList = pp.DowntimeFormsPowerPlansPeds.ToList();

                if (headers.Contains("ID"))
                {
                    linkId = headers.GetValues("ID").First();
                    doc = (from x in documentList where x.Id.ToString() == linkId select x).First();
                }
                else
                {
                    log.Info("api/files/DowntimeForms2016PowerPlansPedsByID-HEADER DID NOT INCLUDE (ID) ");
                }

                System.Data.Services.Client.DataServiceStreamResponse DataServiceStreamResponse = pp.GetReadStream(doc);
                Stream stream = DataServiceStreamResponse.Stream;

                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(stream)
                };

                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = doc.Name
                };

                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

                var response = ResponseMessage(result);

                return response;
            }
            catch (Exception ex)
            {
                log.Info("api/files/DowntimeForms2016PowerPlansPeds/POST", ex);
                return BadRequest();
            }
        }

        /// <summary>
        /// Get DowntimeForms2016PowerPlansAdults By ServiceLineValue based on the ServiceLineValue string passed via Headers.
        /// </summary>
        [HttpGet]
        [Route("DowntimeForms2016PowerPlansAdultsByServiceLineValue")]
        public IHttpActionResult retrieveDowntimeForms2016PowerPlansAdultsItemFilesByServiceLineValue()
        {
            var headers = Request.Headers;
            string ServiceLineValue;
            List<DowntimePlans2016SVC.DowntimeFormsPowerPlansAdultsItem> documentList = new List<DowntimePlans2016SVC.DowntimeFormsPowerPlansAdultsItem>();
            List<DowntimePlans2016SVC.DowntimeFormsPowerPlansAdultsItem> docsByServiceLineValue = new List<DowntimePlans2016SVC.DowntimeFormsPowerPlansAdultsItem>();
            DowntimePlans2016SVC.InformaticsDataContext pp = new DowntimePlans2016SVC.InformaticsDataContext(new Uri(DowntimePlans2016Url));
            pp.Credentials = CredentialCache.DefaultCredentials;
            documentList = pp.DowntimeFormsPowerPlansAdults.ToList();
            try
            {
                if (headers.Contains("ServiceLineValue"))
                {
                    ServiceLineValue = headers.GetValues("ServiceLineValue").First();
                    docsByServiceLineValue = (from x in documentList where x.ServiceLineValue == ServiceLineValue select x).ToList();
                }
                else
                {
                    log.Info("api/files/DowntimeForms2016PowerPlansAdultsByServiceLineValue-HEADER DID NOT INCLUDE (ServiceLineValue) ");
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/DowntimeForms2016PowerPlansAdultsByServiceLineValue", ex);
                return BadRequest();
            }     
            return Ok(docsByServiceLineValue);
        }

        #endregion
    }
}