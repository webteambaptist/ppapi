﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Configuration;
using System.Net.Http;
using System.IO;
using PPAPI.Models;
using PPAPI.DowntimePlansSVC;
using PPAPI.OnCallSVC;
using PPAPI.Bylaws2016SVC;
using System.Net.Http.Headers;
using PPAPI.Helpers;
using System.Security.Permissions;
using System.Runtime.InteropServices;
using System.Runtime.ConstrainedExecution;
using Microsoft.Win32.SafeHandles;
using System.Security;
using System.Text;
using System.Security.Principal;
using System.Data.Services.Client;
using PhysicianPortal.Models;

namespace PPAPI.Controllers
{
    [RoutePrefix("api/files")]
    public class ORScheduleController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        string ORSchedUserName = ConfigurationManager.AppSettings["ORSchedUserName"].ToString();
        string ORSchedPassword = ConfigurationManager.AppSettings["ORSchedPassword"].ToString();
        string ORSchedDomain = ConfigurationManager.AppSettings["ORSchedDomain"].ToString();
        string ORSchedDirectory = ConfigurationManager.AppSettings["ORSchedDirectory"].ToString();

        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword,
        int dwLogonType, int dwLogonProvider, out SafeTokenHandle phToken);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public extern static bool CloseHandle(IntPtr handle);

        [HttpPost]
        [Route("GetORSchedulePDFByFilename")]
        public IHttpActionResult GetORSchedulePDFByFilename()
        {
            SafeTokenHandle safeTokenHandle;
            string[] files;
            List<ORSchedules> schedules = new List<ORSchedules>();
            string filename = null;
            try
            {
                var headers = Request.Headers;
                var pdfbytes = new byte[0];

                if (headers.Contains("FILENAME"))
                {
                    filename = headers.GetValues("FILENAME").First();
                }
                const int LOGON32_PROVIDER_DEFAULT = 0;
                //This parameter causes LogonUser to create a primary token.
                const int LOGON32_LOGON_INTERACTIVE = 2;
                bool returnValue = LogonUser(ORSchedUserName, ORSchedDomain, ORSchedPassword, LOGON32_LOGON_INTERACTIVE,
                    LOGON32_PROVIDER_DEFAULT, out safeTokenHandle);
                StringBuilder text = new StringBuilder();
                if (false == returnValue)
                {
                    int ret = Marshal.GetLastWin32Error();
                    throw new System.ComponentModel.Win32Exception(ret);
                }

                using (safeTokenHandle)
                {
                    using (WindowsIdentity newId = new WindowsIdentity(safeTokenHandle.DangerousGetHandle()))
                    {
                        using (WindowsImpersonationContext impersonatedUser = newId.Impersonate())
                        {
                            files = Directory.GetFiles(ORSchedDirectory);

                            foreach (var item in files)
                            {
                                string[] words = item.Split('\\');
                                string currentfile = words[words.Length - 1].ToString();
                                if (!string.IsNullOrEmpty(filename)&&currentfile.Equals(filename))
                                {
                                    // convert the file to pdf
                                    string html = File.ReadAllText(item);
                                    text = new StringBuilder();
                                    text.Append("<!DOCTYPE html><head></head><meta charset=\"UTF-8\"><html><body><div>");
                                    text.Append("<pre>");
                                    text.Append(html.ToHtml());
                                    text.Append("</pre>");
                                    text.Append("</div></body></html>");
                                    pdfbytes = PDFGenerator.generatePDF(text.ToString());
                                    string newfilename = filename.Substring(0, filename.Length - 4);
                                    filename = newfilename += ".pdf";
                                }
                            }
                        }
                    }
                }
                if (pdfbytes.Length > 0)
                {
                    MemoryStream ms = new MemoryStream(pdfbytes);


                    var result = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StreamContent(ms)
                    };
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = filename
                    };
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                    var response = ResponseMessage(result);


                    return response;
                }
                else
                {
                    return BadRequest("Zero Bytes in file");
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/GetORSchedulePDFByFilename", ex);
                return BadRequest();
            }
        }


        [HttpGet]
        [Route("GetORSchedulesPDF")]
        public IHttpActionResult GetORSchedulesPDF()
        {
            SafeTokenHandle safeTokenHandle;
            string[] files;
            List<ORSchedules> schedules = new List<ORSchedules>();
            OperatingRoom or = new OperatingRoom();
            or.schedules = new List<ORSchedules>();
            try
            {
                const int LOGON32_PROVIDER_DEFAULT = 0;
                //This parameter causes LogonUser to create a primary token.
                const int LOGON32_LOGON_INTERACTIVE = 2;
                bool returnValue = LogonUser(ORSchedUserName, ORSchedDomain, ORSchedPassword, LOGON32_LOGON_INTERACTIVE,
                    LOGON32_PROVIDER_DEFAULT, out safeTokenHandle);
                StringBuilder text = new StringBuilder();
                
                if (false == returnValue)
                {
                    int ret = Marshal.GetLastWin32Error();
                    throw new System.ComponentModel.Win32Exception(ret);
                }

                using (safeTokenHandle)
                {
                    using (WindowsIdentity newId = new WindowsIdentity(safeTokenHandle.DangerousGetHandle()))
                    {
                        using (WindowsImpersonationContext impersonatedUser = newId.Impersonate())
                        {
                            files = Directory.GetFiles(ORSchedDirectory);
                            foreach (var item in files)
                            {
                                string[] words = item.Split('\\');
                                if (item.Contains("BMCDOR"))
                                {
                                    ORSchedules schedule = new ORSchedules();

                                    schedule.OrName = "Jacksonville Schedule";
                                    schedule.filename = words[words.Length-1].ToString();
                                    or.schedules.Add(schedule);
                                }
                                if (item.Contains("BMCWOR"))
                                {
                                    ORSchedules schedule = new ORSchedules();
                                    schedule.OrName = "Wolfson Schedule";
                                    schedule.filename = words[words.Length - 1].ToString();
                                    or.schedules.Add(schedule);
                                }
                                if (item.Contains("BMCBOR"))
                                {
                                    ORSchedules schedule = new ORSchedules();
                                    schedule.OrName = "Beaches Endoscopy Schedule";
                                    schedule.filename = words[words.Length - 1].ToString();
                                    or.schedules.Add(schedule);
                                }
                                if (item.Contains("BMCB2OR"))
                                {
                                    ORSchedules schedule = new ORSchedules();
                                    schedule.OrName = "Beaches OR Schedule";
                                    schedule.filename = words[words.Length - 1].ToString();
                                    or.schedules.Add(schedule);
                                }
                                if (item.Contains("bmcnor"))
                                {
                                    ORSchedules schedule = new ORSchedules();
                                    schedule.OrName = "Nassau OR Schedule";
                                    schedule.filename = words[words.Length - 1].ToString();
                                    or.schedules.Add(schedule);
                                }
                                if (item.Contains("bmcsor"))
                                {
                                    ORSchedules schedule = new ORSchedules();
                                    schedule.OrName = "South OR Schedule";
                                    schedule.filename = words[words.Length - 1].ToString();
                                    or.schedules.Add(schedule);
                                }
                              
                                
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/GetORSchedulesPDF", ex);
                return BadRequest();
            }

            return Ok(or);
        }
        [HttpGet]
        [Route("GetOrSchedules")]
        // [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        public IHttpActionResult GetOrSchedules()
        {
            SafeTokenHandle safeTokenHandle;
            string[] files;
            List<ORSchedules> schedules = new List<ORSchedules>();
            try
            {
                const int LOGON32_PROVIDER_DEFAULT = 0;
                //This parameter causes LogonUser to create a primary token.
                const int LOGON32_LOGON_INTERACTIVE = 2;
                bool returnValue = LogonUser(ORSchedUserName, ORSchedDomain, ORSchedPassword, LOGON32_LOGON_INTERACTIVE,
                    LOGON32_PROVIDER_DEFAULT, out safeTokenHandle);
                StringBuilder text = new StringBuilder();
                if (false == returnValue)
                {
                    int ret = Marshal.GetLastWin32Error();
                    throw new System.ComponentModel.Win32Exception(ret);
                }

                using (safeTokenHandle)
                {
                    using (WindowsIdentity newId = new WindowsIdentity(safeTokenHandle.DangerousGetHandle()))
                    {
                        using (WindowsImpersonationContext impersonatedUser = newId.Impersonate())
                        {
                            files = Directory.GetFiles(ORSchedDirectory);

                            foreach (var item in files)
                            {
                                if (item.Contains("BMCDOR"))
                                {
                                    ORSchedules schedule = new ORSchedules();
                                    schedule.OrName = "Jacksonville Schedule";
                                    string value = File.ReadAllText(item);
                                    text.Append("<pre>");
                                    text.Append(value.ToHtml());
                                    text.Append("</pre>");
                                    schedule.filename = text.ToString();
                                    schedules.Add(schedule);
                                }
                                if (item.Contains("BMCWOR"))
                                {
                                    ORSchedules schedule = new ORSchedules();
                                    schedule.OrName = "Wolfson Schedule";
                                    string value = File.ReadAllText(item);
                                    text = new StringBuilder();
                                    text.Append("<pre>");
                                    text.Append(value.ToHtml());
                                    text.Append("</pre>");
                                    schedule.filename = text.ToString();
                                    schedules.Add(schedule);
                                }
                                if (item.Contains("BMCBOR"))
                                {
                                    ORSchedules schedule = new ORSchedules();
                                    schedule.OrName = "Beaches Endoscopy Schedule";
                                    string value = File.ReadAllText(item);
                                    text = new StringBuilder();
                                    text.Append("<pre>");
                                    text.Append(value.ToHtml());
                                    text.Append("</pre>");
                                    schedule.filename = text.ToString();
                                    schedules.Add(schedule);
                                }
                                if (item.Contains("BMCB2OR"))
                                {
                                    ORSchedules schedule = new ORSchedules();
                                    schedule.OrName = "Beaches OR Schedule";
                                    string value = File.ReadAllText(item);
                                    text = new StringBuilder();
                                    text.Append("<pre>");
                                    text.Append(value.ToHtml());
                                    text.Append("</pre>");
                                    schedule.filename = text.ToString();
                                    schedules.Add(schedule);
                                }
                                if (item.Contains("bmcnor"))
                                {
                                    ORSchedules schedule = new ORSchedules();
                                    schedule.OrName = "Nassau OR Schedule";
                                    string value = File.ReadAllText(item);
                                    text = new StringBuilder();
                                    text.Append("<pre>");
                                    text.Append(value.ToHtml());
                                    text.Append("</pre>");
                                    schedule.filename = text.ToString();
                                    schedules.Add(schedule);
                                }
                                if (item.Contains("bmcsor"))
                                {
                                    ORSchedules schedule = new ORSchedules();
                                    schedule.OrName = "South OR Schedule";
                                    string value = File.ReadAllText(item);
                                    text = new StringBuilder();
                                    text.Append("<pre>");
                                    text.Append(value.ToHtml());
                                    text.Append("</pre>");
                                    schedule.filename = text.ToString();
                                    schedules.Add(schedule);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/GetOrSchedules", ex);
                return BadRequest();
            }

            return Ok(schedules);
        }
        // <summary>
        // Get OR Schedules 
        // </summary>
        [HttpGet]
        [Route("GetOrSchedule/{ORName}")]
        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        public IHttpActionResult GetOrSchedule(string ORName)
        {
            SafeTokenHandle safeTokenHandle;
            string[] files;

            try
            {
                const int LOGON32_PROVIDER_DEFAULT = 0;
                //This parameter causes LogonUser to create a primary token.
                const int LOGON32_LOGON_INTERACTIVE = 2;
                bool returnValue = LogonUser(ORSchedUserName, ORSchedDomain, ORSchedPassword, LOGON32_LOGON_INTERACTIVE,
                    LOGON32_PROVIDER_DEFAULT, out safeTokenHandle);
                StringBuilder text = new StringBuilder();
                if (false == returnValue)
                {
                    int ret = Marshal.GetLastWin32Error();
                    throw new System.ComponentModel.Win32Exception(ret);
                }

                using (safeTokenHandle)
                {
                    using (WindowsIdentity newId = new WindowsIdentity(safeTokenHandle.DangerousGetHandle()))
                    {
                        using (WindowsImpersonationContext impersonatedUser = newId.Impersonate())
                        {
                            files = Directory.GetFiles(ORSchedDirectory);

                            if (ORName == "Jacksonville Schedule")
                            {
                                // get text from file
                                foreach (var item in files)
                                {
                                    if (item.Contains("BMCDOR"))
                                    {
                                        string value = File.ReadAllText(item);
                                        text.Append("<Html><head></head><body><pre>");
                                        text.Append(value.ToHtml());
                                        text.Append("</pre></body></Html>");
                                    }
                                }
                            }
                            if (ORName == "Wolfson Schedule")
                            {
                                foreach (var item in files)
                                {
                                    if (item.Contains("BMCWOR"))
                                    {
                                        string value = File.ReadAllText(item);
                                        text.Append("<Html><head></head><body><pre>");
                                        text.Append(value.ToHtml());
                                        text.Append("</pre></body></Html>");
                                    }
                                }
                            }
                            if (ORName == "Beaches Endoscopy Schedule")
                            {
                                foreach (var item in files)
                                {
                                    if (item.Contains("BMCBOR"))
                                    {
                                        string value = File.ReadAllText(item);
                                        text.Append("<Html><head></head><body><pre>");
                                        text.Append(value.ToHtml());
                                        text.Append("</pre></body></Html>");
                                    }
                                }
                            }
                            if (ORName == "Beaches OR Schedule")
                            {
                                foreach (var item in files)
                                {
                                    if (item.Contains("BMCB2OR"))
                                    {
                                        string value = File.ReadAllText(item);
                                        text.Append("<Html><head></head><body><pre>");
                                        text.Append(value.ToHtml());
                                        text.Append("</pre></body></Html>");
                                    }
                                }
                            }
                            if (ORName == "Nassau OR Schedule")
                            {
                                foreach (var item in files)
                                {
                                    if (item.Contains("BMCNOR"))
                                    {
                                        string value = File.ReadAllText(item);
                                        text.Append("<Html><head></head><body><pre>");
                                        text.Append(value.ToHtml());
                                        text.Append("</pre></body></Html>");
                                    }
                                }
                            }
                            if (ORName == "South OR Schedule")
                            {
                                foreach (var item in files)
                                {
                                    if (item.Contains("BMCSOR"))
                                    {
                                        string value = File.ReadAllText(item);
                                        text.Append("<Html><head></head><body><pre>");
                                        text.Append(value.ToHtml());
                                        text.Append("</pre></body></Html>");
                                    }
                                }
                            }
                        }
                    }
                }

                return Ok(text.ToString());
            }
            catch (Exception ex)
            {
                log.Info("api/files/GetOrSchedule/{ORName}",ex);
                return BadRequest();
            }

        }
        /// <summary>
        /// Encodes a single paragraph to HTML.
        /// </summary>
        /// <param name="s">Text to encode</param>
        /// <param name="sb">StringBuilder to write results</param>
        /// <param name="nofollow">If true, links are given "nofollow"
        /// attribute</param>
        private static void EncodeParagraph(string s, StringBuilder sb, bool nofollow)
        {
            // Start new paragraph
            sb.AppendLine("<p>");

            // HTML encode text
            s = HttpUtility.HtmlEncode(s);

            // Convert single newlines to <br>
            s = s.Replace(Environment.NewLine, "<br />\r\n");

            // Close paragraph
            sb.AppendLine("\r\n</p>");
        }
        public sealed class SafeTokenHandle : SafeHandleZeroOrMinusOneIsInvalid
        {
            private SafeTokenHandle()
                : base(true)
            {
            }

            [DllImport("kernel32.dll")]
            [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
            [SuppressUnmanagedCodeSecurity]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool CloseHandle(IntPtr handle);

            protected override bool ReleaseHandle()
            {
                return CloseHandle(handle);
            }
        }
    }
}