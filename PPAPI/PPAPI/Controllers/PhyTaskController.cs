﻿using PPAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;

namespace PPAPI.Controllers
{    
    /// <summary>
    /// Get tasks for Docs
    /// </summary>
    [RoutePrefix("api/tasks")]
    public class PhyTaskController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// get tasks for a specific Doc
        /// </summary>
        [Route("gettasks")]
        [HttpGet]
        public IHttpActionResult selTasks()
        {
            var headers = Request.Headers;
            string echoID = "";

            try
            {
                var ppEntities = new PhysiciansPortalEntities();
                var tasks = ppEntities.PhysicianTaskLists;

                if (headers.Contains("echoid"))
                {
                    echoID = headers.GetValues("echoid").First();
                }

                if (tasks == null)
                {
                    return Json("");
                }
                else
                {
                    var taks = tasks.Where(x => x.EchoDoctorNumber == echoID);
                    return Json(taks);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/tasks/gettasks", ex);
                return BadRequest();
            }
        }

        /// <summary>
        /// update task as submitted
        /// </summary>
        //[Route("updTask")]
        //[HttpGet]
        //public IHttpActionResult updateTask ()
        //{
        //    var headers = Request.Headers;
        //    string taskID = "";

        //    try
        //    {
        //        var ppEntities = new PhysiciansPortalEntities();
        //        var tasks = ppEntities.PhysicianTaskLists;

        //        if (headers.Contains("taskid"))
        //        {
        //            taskID = headers.GetValues("taskid").First();
        //        }

        //        if (tasks == null)
        //        {
        //            return Json("");
        //        }
        //        else
        //        {
        //           ppEntities.updTask (int.Parse(taskID), DateTime.Now);
        //            return Ok();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Info("api/tasks/updTask", ex);
        //        return BadRequest();
        //    }
        //}

        [Route("updTask")]
        [HttpPost]
        public IHttpActionResult updateTask()
        {
            var headers = Request.Headers;
            string taskID = "";
            string comments = "";
            string file = "";

            try
            {
                var ppEntities = new PhysiciansPortalEntities();
                var tasks = ppEntities.PhysicianTaskLists;

                if (headers.Contains("taskid"))
                {
                    taskID = headers.GetValues("taskid").First();
                }
                if (headers.Contains("comments"))
                {
                    comments = headers.GetValues("comments").First();
                }
                if (headers.Contains("file"))
                {
                    file = headers.GetValues("file").First();
                }

                if (tasks == null)
                {
                    return Json("");
                }
                else
                {
                    var id = Convert.ToInt32(taskID);
                    var task = ppEntities.PhysicianTaskLists.FirstOrDefault(x => x.ID == id);
                    task.IsSubmitted = true;
                    task.SubmitDate = DateTime.Now;
                    ppEntities.SaveChanges();

                    //Generate Email

                    GenerateEmail(taskID, comments, file);

                    //ppEntities.updTask(int.Parse(taskID), DateTime.Now);
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                log.Info("api/tasks/updTask", ex);
                return BadRequest();
            }
        }

        private void GenerateEmail(string taskID, string comments, string file)
        {
            try
            {
                if (file == "")
                {
                    file = null;
                }
                var ppEntities = new PhysiciansPortalEntities();
                var id = Convert.ToInt32(taskID);
                var taskInfo = ppEntities.PhysicianTaskLists.FirstOrDefault(x => x.ID == id);

                var client = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Host = ConfigurationManager.AppSettings["RELAY"],
                    EnableSsl = false
                };

                var bodymessage = "<p>A Completed Task Submission has been received.</p><p>Task Information</p>" +
                    "<p>Submitter Name - " + taskInfo.DoctorName + "</p>" +
                    "<p>Task Name - " + taskInfo.Task + "</p>" +
                    "<p>Task ID - " + taskID + "</p>" +
                    "<p>Additional Comments - " + comments + "</p>" +
                    "<p>(Please check for any attachments to this email)</p>";

                var m = new MailMessage
                {
                    From = new MailAddress(ConfigurationManager.AppSettings["sender"])
                };
                m.To.Add(ConfigurationManager.AppSettings["recipient"]);
                m.Subject = "Subject Completed Task Submission";
                m.Body = bodymessage;

                var filePath = "";

                if (!string.IsNullOrEmpty(file))
                {
                    var fileDirectory = ConfigurationManager.AppSettings["fileDirectory"];
                    filePath = fileDirectory + file;
                    m.Attachments.Add(new Attachment(filePath));
                }

                m.IsBodyHtml = true;
                client.Send(m);

                m.Attachments.Dispose();
                File.Delete(filePath);

            }
            catch (Exception ex)
            {
                log.Info("api/tasks generating email", ex);
            }
        }
    }
}
