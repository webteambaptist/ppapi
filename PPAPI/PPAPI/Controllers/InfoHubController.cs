﻿using PPAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PPAPI.Controllers
{
    /// <summary>
    /// CRUD for InfoHub
    /// </summary>
     
    [RoutePrefix("api/info")]
    public class InfoHubController : ApiController
    {
        /// <summary>
        /// insert new infohubs
        /// </summary>
        /// <param name="infoList"></param>
        /// <returns></returns>
        [Route("saveInfo")]
        [HttpPut]
        public IHttpActionResult saveInfo([FromBody] List<PhysicianInfoHub> infoList)
        {
            try
            {
                var ppEntities = new PhysiciansPortalEntities();
                var infos = ppEntities.PhysicianInfoHubs;

                if (infoList == null)
                {
                    return Json("No Data");
                }
                else
                {
                   foreach (var p in infoList)
                    {
                        //var g = new PhysicianInfoHub();

                        //g.ECHOID = p.ECHOID;

                        try
                        {
                            DateTime.Parse(p.ReadDate.ToString());
                        }
                        catch (Exception ex)
                        {
                            p.ReadDate = DateTime.Now;
                        }

                        //g.ContentID = p.ContentID;

                        ppEntities.insInfo2(p.ECHOID, p.ContentID, p.ReadDate);
                    }

                  //  ppEntities.SaveChanges();
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// get info
        /// </summary>
        /// <returns></returns>
        [Route("getinfo")]
        [HttpGet]
        public IHttpActionResult selInfo()
        {
            var headers = Request.Headers;
            string echoID = "";

            try
            {
                var ppEntities = new PhysiciansPortalEntities();
                var info = ppEntities.PhysicianInfoHubs;

                if (headers.Contains("echoid"))
                {
                    echoID = headers.GetValues("echoid").First();
                }

                if (info == null)
                {
                    return Json("No Data");
                }
                else
                {
                    var drinfo = info.Where(x => x.ECHOID == echoID);
                    return Json(drinfo);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// update read date
        /// </summary>
        [Route("updInfo")]
        [HttpGet]
        public IHttpActionResult updateInfo()
        {
            var headers = Request.Headers;
            string ID = "";

            try
            {
                var ppEntities = new PhysiciansPortalEntities();
                var tasks = ppEntities.PhysicianInfoHubs;

                if (headers.Contains("id"))
                {
                    ID = headers.GetValues("id").First();
                }

                if (tasks == null)
                {
                    return Json("No Data");
                }
                else
                {
                    ppEntities.updInfo(int.Parse(ID), DateTime.Now);
                    return Ok();
                }
            }
            catch (Exception ee)
            {
                return BadRequest();
            }
        }
    }
}
