﻿using PPAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PPAPI.Controllers
{
    [RoutePrefix("api/oncall")]
    public class OnCallController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("getschedule")]
        [HttpGet]
        public IHttpActionResult getSchedule()
        {

            try
            {
                var ppEntities = new PhysiciansPortalEntities();
                var hospitals = ppEntities.Hospitals;

                List<Schedule> schedules = new List<Schedule>();

                foreach (var s in hospitals)
                {
                    Schedule schedule = new Schedule();
                    schedule.ID = s.ID;
                    schedule.facility = s.Facility;

                    var departments = ppEntities.Departments.Where(x => x.FacilityId == s.ID).ToList();

                    List<Department> deps = new List<Department>();
                    foreach (var d in departments)
                    {
                        Department dep = new Department();
                        dep.ID = d.ID;
                        dep.Link = d.Link;
                        dep.FacilityId = s.ID;
                        dep.Department1 = d.Department1;
                        deps.Add(dep);
                    }
                    schedule.departments = deps;
                    schedules.Add(schedule);
                }


                return Json(schedules);
                
            } catch (Exception ex)
            {
                log.Info("api/oncall/getschedule", ex);
                return BadRequest(ex.Message);
            }
        }
    }
}
