﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Configuration;
using System.Net.Http;
using System.IO;
using PPAPI.Models;
using PPAPI.DowntimePlansSVC;
using PPAPI.OnCallSVC;
using PPAPI.Bylaws2016SVC;
using System.Net.Http.Headers;
using PPAPI.Helpers;
using System.Security.Permissions;
using System.Runtime.InteropServices;
using System.Runtime.ConstrainedExecution;
using Microsoft.Win32.SafeHandles;
using System.Security;
using System.Text;
using System.Security.Principal;

namespace PPAPI.Controllers
{
    [RoutePrefix("api/files")]
    public class Bylaws2016SVCController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string Bylaws2016Url = ConfigurationManager.AppSettings["Bylaws2016"].ToString();

        #region BylawsAPI
        /// <summary>
        /// Get BylawsRulesRegsDocumentClassificationValue that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("BylawsRulesRegsDocumentClassificationValueFiles")]
        public IHttpActionResult retrieveBylawsRulesRegsDocumentClassificationValueFiles()
        {
            List<Bylaws2016SVC.BylawsRulesRegsDocumentClassificationValue> docs = new List<Bylaws2016SVC.BylawsRulesRegsDocumentClassificationValue>();

            try
            {
                //ClientContext ctx = new ClientContext("https://qaportal.bmcjax.com/Teams/QaPp");
                //http://spphysicians2010.e-baptisthealth.com/schedules/_vti_bin/listdata.svc

                MedicalStaffOfficeDataContext pp = new MedicalStaffOfficeDataContext(new Uri(Bylaws2016Url));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var a in pp.BylawsRulesRegsDocumentClassification)
                {
                    docs.Add(a);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/BylawsRulesRegsDocumentClassificationValueFiles", ex);
                return BadRequest();
            }

            return Ok(docs);
        }

        /// <summary>
        /// Get BylawsRulesRegsFacilityValue that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("BylawsRulesRegsFacilityValue")]
        public IHttpActionResult retrieveBylawsRulesRegsFacilityValueFiles()
        {
            List<Bylaws2016SVC.BylawsRulesRegsFacilityValue> docs = new List<Bylaws2016SVC.BylawsRulesRegsFacilityValue>();

            try
            {
                MedicalStaffOfficeDataContext pp = new MedicalStaffOfficeDataContext(new Uri(Bylaws2016Url));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var a in pp.BylawsRulesRegsFacility)
                {
                    docs.Add(a);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/BylawsRulesRegsFacilityValue", ex);
                return BadRequest();
            }

            return Ok(docs);
        }

        /// <summary>
        /// Get BylawsRulesRegsItem that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("BylawsRulesRegsItem")]
        public IHttpActionResult retrieveBylawsRulesRegsItemFiles()
        {
            List<Bylaws2016SVC.BylawsRulesRegsItem> docs = new List<Bylaws2016SVC.BylawsRulesRegsItem>();

            try
            {
                MedicalStaffOfficeDataContext pp = new MedicalStaffOfficeDataContext(new Uri(Bylaws2016Url));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var a in pp.BylawsRulesRegs)
                {
                    docs.Add(a);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/BylawsRulesRegsItem", ex);
                return BadRequest();
            }

            return Ok(docs);
        }

        /// <summary>
        /// Post BylawsRulesRegsItem PDF based on the ID
        /// </summary>
        [HttpPost]
        [Route("BylawsRulesRegsItem/{id}")]
        public IHttpActionResult retrieveBylawsRulesRegsItemFilesPDFByID(int id)
        {
            try
            {
                var headers = Request.Headers;
                int linkId = id;

                MedicalStaffOfficeDataContext pp = new MedicalStaffOfficeDataContext(new Uri(Bylaws2016Url));
                pp.Credentials = CredentialCache.DefaultCredentials;

                List<Bylaws2016SVC.BylawsRulesRegsItem> documentList = new List<Bylaws2016SVC.BylawsRulesRegsItem>();
                documentList = pp.BylawsRulesRegs.ToList();
                var doc = (from x in documentList where x.Id == linkId select x).First();

                System.Data.Services.Client.DataServiceStreamResponse DataServiceStreamResponse = pp.GetReadStream(doc);
                Stream stream = DataServiceStreamResponse.Stream;

                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(stream)
                };

                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = doc.Name
                };

                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

                var response = ResponseMessage(result);

                return response;
            }
            catch(Exception ex)
            {
                log.Info("api/files/BylawsRulesRegsItem/{id}", ex);
                return BadRequest();
            }
        }
        #endregion
    }
}