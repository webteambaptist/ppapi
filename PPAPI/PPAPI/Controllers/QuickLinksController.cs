﻿using PPAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PPAPI.Controllers
{
    /// <summary>
    /// CRUD API for QuickLinks
    /// </summary>
    [RoutePrefix("api/qlinks")]
    public class QuickLinksController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// get quicklinks by physician
        /// </summary>
        [Route("getlinks")]
        [HttpGet]
        public IHttpActionResult selLinks()
        {
            var headers = Request.Headers;
            string echoID = "";

            try
            {
                var ppEntities = new PhysiciansPortalEntities();
                var qLinks = ppEntities.PhysicianQuicklinks;

                if (headers.Contains("echoid"))
                {
                    echoID = headers.GetValues("echoid").First();
                }

                if (qLinks == null)
                {
                    return Json("");
                }
                else
                {
                    var links = qLinks.Where(x => x.ECHOID == echoID);
                    return Json(links);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/qlinks/getlinks", ex);
                return BadRequest();
            }
        }

        //delete and update quicklinks by physician

        /// <summary>
        /// save/update links
        /// </summary>
        [Route("savelinks")]
        [HttpPost]
        public IHttpActionResult updateLinks(List<PhysicianQuicklink> qLinks)
        {
            var ppEntities = new PhysiciansPortalEntities();
            var pLinks = ppEntities.PhysicianQuicklinks;

            //delete old links, get echoid of first element, assume qlinks is a single provider's links
            try
            {
                if (qLinks != null)
                {
                    string echoID = qLinks.First().ECHOID;
                    ppEntities.delQuickLinks(echoID);

                    foreach (PhysicianQuicklink q in qLinks)
                    {
                        if ((q.LinkTitle != null) && (q.LinkURL != null))
                        {
                            pLinks.Add(q);
                        }
                    }
                    ppEntities.SaveChanges();
                }
                return Ok();
            }
            catch(Exception ex)
            {
                log.Info("api/qlinks/savelinks", ex);
                return BadRequest(ex.InnerException.Message);
            }

        }
    }
}
