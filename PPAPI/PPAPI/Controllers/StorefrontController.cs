﻿using Newtonsoft.Json.Linq;
using PPAPI.Helpers;
using PPAPI.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace PPAPI.Controllers
{
    /// <summary>
    /// API for Storefront/Citrix stuff
    /// </summary>
    
    [RoutePrefix("api/sf")]
    public class StorefrontController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //static string baseSFURL = "devsf.bmcjax.com/Citrix/BaptistTestWeb/";
        static string baseSFURL = ConfigurationManager.AppSettings["URL"]; //"sf.bmcjax.com/Citrix/BaptistWeb"; 
        static string fileSFURL = ConfigurationManager.AppSettings["FURL"]; 

        /// <summary>
        /// authenticate, return session level data
        /// </summary>
        /// <returns></returns>
        [Route("authenticate")]
        [HttpGet]
        public IHttpActionResult authenticate()
        {
            log.Info("authenticate STARTED");
            var headers = Request.Headers;

            try
            {
                if (headers.Contains("scuser"))
                {
                    //assume BH is the domain
                    //get cookie, split at ':', decrypt password
                    string cookie = headers.GetValues("scuser").First(); //get from header
                    var cookiePieces = cookie.Split(':');
                    if (cookiePieces.Length > 1)
                    {
                        //always use SSL!
                        Dictionary<string, string> _returnValues = new Dictionary<string, string>();
                        string _csrfToken = Guid.NewGuid().ToString();
                        string _aspnetSessionID = Guid.NewGuid().ToString();
                        string _username = cookiePieces[0];
                        var pwd = cookiePieces[1].ToString();
                        string _password = Crypto.Base64Decode(pwd);
                        // string _password = Crypto.Decrypt(cookiePieces[1]); //cookiePieces[1];
                        string _domain = "bh";
                        string SFURL = "https://" + baseSFURL;
                        log.Info("SFURL:" + SFURL);
                        if (_username.StartsWith("ad\\", true, null))
                        {
                            _username = _username.Substring(3);
                        }

                        string _authenticationBody = string.Format("username={0}\\{1}&password={2}", _domain, _username, HttpUtility.UrlEncode(_password));
                        log.Info("username is " + _username);
                        RestClient _rc = new RestClient(SFURL);
                        log.Info("SFURL:" + SFURL);

                        RestRequest _authReq = new RestRequest("/PostCredentialsAuth/Login", Method.POST);
                        //if (UseSSL)
                        //{
                        _authReq.AddHeader("X-Citrix-IsUsingHTTPS", "Yes");
                        //}
                        //else
                        //{
                        //    _authReq.AddHeader("X-Citrix-IsUsingHTTPS", "No");
                        //}
                        _authReq.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                        _authReq.AddHeader("Csrf-Token", _csrfToken);
                        _authReq.AddCookie("csrftoken", _csrfToken);
                        _authReq.AddCookie("asp.net_sessionid", _aspnetSessionID);
                        _authReq.AddParameter("application/x-www-form-urlencoded", _authenticationBody, ParameterType.RequestBody);

                        RestSharp.IRestResponse _authResp = _rc.Execute(_authReq);
                        log.Info("Checking for headers where Name == Set-Cookie...");
                        foreach (var header in _authResp.Headers.Where(i => i.Name == "Set-Cookie"))
                        {
                            log.Info(header.Name);
                            Console.WriteLine(header.Name);
                            log.Info("Checking for cookieValues...");                            
                            string[] cookieValues = header.Value.ToString().Split(',');
                                                        
                            foreach (string cookieValue in cookieValues)
                            {
                                log.Info("Adding cookieValue = " + cookieValue);
                                try
                                {
                                    string[] cookieElements = cookieValue.Split(';');
                                    string[] keyValueElements = cookieElements[0].Split('=');
                                    _returnValues.Add(keyValueElements[0], keyValueElements[1]); //1
                                }
                                catch (Exception ex)
                                {
                                    log.Info("StorefrontController/authenticate(), ERROR adding cookieValue", ex);                                    
                                    continue;
                                }
                            }
                        }
                        log.Info("authenticate FINISHED");
                        log.Info("_returnvalues: " + _returnValues);
                        return Json(_returnValues);
                    }
                    else
                    {
                        log.Info("StorefrontController/authenticate(); AUTHENTICATION FAILED: CookiePieces.Length is NOT > 1. CookiePieces.Length = " + cookiePieces.Length);
                        return Json("Missing Data");
                    }
                }
                else
                {
                    log.Info("StorefrontController/authenticate(); AUTHENTICATION FAILED: HEADER does NOT contain scuser.");
                    return Json("Missing Data");
                }
            }
            catch(Exception ex)
            {
                log.Info("authenticate FAILED");
                log.Info("api/sf/authenticate", ex);
                return BadRequest(ex.Message);
            }
        }
 
        /// <summary>
        /// all in one method to return an ica for testing
        /// </summary>
        /// <returns></returns>
        [Route("oneshot")]
        [HttpGet]
        public IHttpActionResult oneshot()
        {
            log.Info("authenticate STARTED");
            var headers = Request.Headers;

            try
            {
                if (headers.Contains("scuser"))
                {
                    //assume BH is the domain
                    //get cookie, split at ':', decrypt password
                    string cookie = headers.GetValues("scuser").First(); //get from header
                    var cookiePieces = cookie.Split(':');
                    if (cookiePieces.Length > 1)
                    {
                        //always use SSL!
                        Dictionary<string, string> _returnValues = new Dictionary<string, string>();
                        string _csrfToken = Guid.NewGuid().ToString();
                        string _aspnetSessionID = Guid.NewGuid().ToString();
                        string _username = cookiePieces[0];
                        string _password = Crypto.Decrypt(cookiePieces[1]); //cookiePieces[1];
                        string _domain = "bh";
                        string SFURL = "https://" + baseSFURL;
                        log.Info("SFURL:" + SFURL);
                        if (_username.StartsWith("ad\\", true, null))
                        {
                            _username = _username.Substring(3);
                        }

                        string _authenticationBody = string.Format("username={0}\\{1}&password={2}", _domain, _username, _password);
                        log.Info("username is " + _username);
                        RestClient _rc = new RestClient(SFURL);
                        log.Info("SFURL:" + SFURL);

                        RestRequest _authReq = new RestRequest("/PostCredentialsAuth/Login", Method.POST);
                        //if (UseSSL)
                        //{
                        _authReq.AddHeader("X-Citrix-IsUsingHTTPS", "Yes");
                        //}
                        //else
                        //{
                        //    _authReq.AddHeader("X-Citrix-IsUsingHTTPS", "No");
                        //}
                        _authReq.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                        _authReq.AddHeader("Csrf-Token", _csrfToken);
                        _authReq.AddCookie("csrftoken", _csrfToken);
                        _authReq.AddCookie("asp.net_sessionid", _aspnetSessionID);
                        _authReq.AddParameter("application/x-www-form-urlencoded", _authenticationBody, ParameterType.RequestBody);

                        RestSharp.IRestResponse _authResp = _rc.Execute(_authReq);
                        log.Info("Checking for headers where Name == Set-Cookie...");
                        foreach (var header in _authResp.Headers.Where(i => i.Name == "Set-Cookie"))
                        {
                            log.Info(header.Name);
                            Console.WriteLine(header.Name);
                            log.Info("Checking for cookieValues...");
                            string[] cookieValues = header.Value.ToString().Split(',');

                            foreach (string cookieValue in cookieValues)
                            {
                                log.Info("Adding cookieValue = " + cookieValue);
                                try
                                {
                                    string[] cookieElements = cookieValue.Split(';');
                                    string[] keyValueElements = cookieElements[0].Split('=');
                                    _returnValues.Add(keyValueElements[0], keyValueElements[1]); //1
                                }
                                catch (Exception ex)
                                {
                                    log.Info("StorefrontController/authenticate(), ERROR adding cookieValue", ex);
                                    continue;
                                }
                            }
                        }
                        log.Info("authenticate FINISHED");
                        log.Info("_returnvalues: " + _returnValues);
                        return Json(_returnValues);
                    }
                    else
                    {
                        log.Info("StorefrontController/authenticate(); AUTHENTICATION FAILED: CookiePieces.Length is NOT > 1. CookiePieces.Length = " + cookiePieces.Length);
                        return Json("Missing Data");
                    }
                }
                else
                {
                    log.Info("StorefrontController/authenticate(); AUTHENTICATION FAILED: HEADER does NOT contain scuser.");
                    return Json("Missing Data");
                }
            }
            catch (Exception ex)
            {
                log.Info("authenticate FAILED");
                log.Info("api/sf/authenticate", ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// get list of user specific apps
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        [Route("getList")]
        [HttpPost]
        public IHttpActionResult getList([FromBody] ResourceModel res)
        {
            if (res == null) //check headers
            {
                try
                {
                    var headers = Request.Headers;

                    if (headers != null)
                    {
                        res = new ResourceModel();
                        res.SessionID = headers.GetValues("SessionID").First();
                        res.CsrfToken = headers.GetValues("CsrfToken").First();
                        res.AuthID = headers.GetValues("CtxsAuthId").First();

                    }
                }
                catch (Exception ex)
                {
                    return BadRequest("Missing Headers");
                }
            }



            try
            {
                log.Info("getList STARTED");
                var newLine = Environment.NewLine;
                List<CitrixApplicationInfo> _applicationList = new List<CitrixApplicationInfo>();

                string SFURL = "https://" + baseSFURL;

                RestClient _rc = new RestClient(SFURL);
                RestRequest _getResourcesReq = new RestRequest(@"Resources/List", Method.POST);
                log.Info("SFURL:" + SFURL);
                log.Info("Adding headerds to _getResourcesReq...");
                _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "Yes");
                log.Info("X-Citrix-IsUsingHTTPS header ADDED");
                _getResourcesReq.AddHeader("Accept", "application/json");
                //log.Info("Accept header ADDED");
                _getResourcesReq.AddHeader("Csrf-Token", res.CsrfToken);
               // log.Info("Csrf-Token header ADDED");
                _getResourcesReq.AddCookie("csrftoken", res.CsrfToken);
                //log.Info("csrftoken header ADDED");
                _getResourcesReq.AddCookie("asp.net_sessionid", res.SessionID);
                //log.Info("asp.net_sessionid header ADDED");
                _getResourcesReq.AddCookie("CtxsAuthId", res.AuthID);
                //log.Info("CtxsAuthId header ADDED");
                _getResourcesReq.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                IRestResponse _resourceListResp = _rc.Execute(_getResourcesReq);

                string json = _resourceListResp.Content;

                JObject a = JObject.Parse(json);
                JArray resources = (JArray)a["resources"];

                var ppEntities = new PhysiciansPortalEntities();
                var apps = ppEntities.AppImageMatrices;
                var images = ppEntities.Images;

                log.Info("Checking resources...");

                foreach (var o in resources)
                {
                    log.Info("Checking resource: " + o);
                    CitrixApplicationInfo r = new CitrixApplicationInfo();

                    try
                    {
                        r.AppDesc = o["description"].ToString();
                    }
                    catch (Exception e)
                    {
                        r.AppDesc = "";
                    }

                    //new code to handle external AppIcons

                    //do match on

                    //load entity, find where name == name in DB, load URL into r.appicon https://ws.bmcjax.com/Storefront/
                    string name = o["name"].ToString();
                    


                    try
                    {
                        r.AppOrder = 9;
                        try
                        {
                            var appmatrix = apps.Where(x => x.AppName == name).First();
                            var imagepath = images.Where(y => y.ID == appmatrix.ImageID).First();


                            try
                            {
                                r.AppOrder = appmatrix.AppOrder;
                            }
                            catch
                            {
                                r.AppOrder = 9;
                            }


                            if (imagepath != null)
                            {
                                r.AppIcon = fileSFURL + imagepath.FileName;

                            }
                            else
                            {
                                r.AppIcon = fileSFURL + "ie.png";
                            }
                            r.AppOrder = appmatrix.AppOrder;
                        }
                        catch(Exception e)
                        {
                            r.AppIcon = fileSFURL + "ie.png";
                        }

                       // log.Info("AppOrder" + appmatrix.AppOrder.ToString());
 
                        r.AppIcon = r.AppIcon.Replace("\r\n", "");
                        r.AppLaunchURL = o["launchurl"].ToString();
                        r.ID = o["id"].ToString();
                        r.AppTitle = o["name"].ToString();
                        var nameContents = o["name"].ToString().ToUpper();
                        _applicationList.Add(r);

                    }
                    catch (Exception ex)
                    {
                        r.AppIcon = fileSFURL + "ie.png";
                    }
                }



                var outList = _applicationList.OrderBy(x => x.AppOrder).ThenBy(x => x.AppTitle);

                return Json(outList);
            }
            catch (Exception ex)
            {
                log.Info("getList FAILED");
                log.Info("api/sf/getList" + ex.InnerException.Message);
                return BadRequest();
            }
        }

        /// <summary>
        /// get ica file
        /// </summary>
        [Route("getIca")]
        [HttpPost]
        public HttpResponseMessage LaunchApplication ([FromBody] ResourceModel res)
        {
            log.Info("getIca STARTED");

            if (res == null) //check headers
            {
                try
                {
                    var headers = Request.Headers;

                    if (headers != null)
                    {
                        res = new ResourceModel();
                        res.SessionID = headers.GetValues("SessionID").First();
                        res.CsrfToken = headers.GetValues("CsrfToken").First();
                        res.AuthID = headers.GetValues("CtxsAuthId").First();
                        res.AppID = headers.GetValues("AppID").First();
                    }
                }
                catch (Exception ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex.InnerException.Message);
                }
            }

            try
            {
                log.Info("getIca, sessionID:" + res.SessionID + " authID:" + res.AuthID + " csrfToken:" + res.CsrfToken + " appID:" + res.AppID);
                string result = GetIcaString(res.SessionID, res.AuthID, res.CsrfToken, res.AppID, true);
                log.Info("result = " + result);
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(result, System.Text.Encoding.UTF8, "text/plain");
                log.Info("getIca FINISHED: " + response.Content.ToString());
                return response;
            }
            catch(Exception ex)
            {                
                //var message = "Exception caught, please check the log file";
                log.Info("getIca FAILED");
                log.Info("api/sf/getIca", ex);
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex.InnerException.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SessionID"></param>
        /// <param name="AuthID"></param>
        /// <param name="CsrfToken"></param>
        /// <param name="AppID"></param>
        /// <param name="UseSSL"></param>
        /// <returns></returns>
        public static string GetIcaString(string SessionID, string AuthID, string CsrfToken, string AppID, bool UseSSL)
        {
            try
            {
                ///Resources/GetLaunchStatus/{id}
                // baseSFURL = "devstorefront.bmcjax.com//Citrix//BaptistTestWeb//";

                string SFURL = null;

                if (UseSSL)
                {
                    SFURL = "https://" + baseSFURL;
                }
                else
                {
                    SFURL = "http://" + baseSFURL;
                }
                log.Info("SFURL:" + SFURL);
                RestClient _rc = new RestClient(SFURL);

                // RestRequest _getResourcesReq = new RestRequest(string.Format("Resources/GetLaunchStatus/{0}", AppID), Method.GET);
                //RestRequest _getResourcesReq = new RestRequest(string.Format("Resources/LaunchIca/{0}", AppID), Method.GET);
                RestRequest _getResourcesReq = new RestRequest(AppID, Method.GET);

                if (UseSSL)
                {
                    _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "Yes");
                }
                else
                {
                    _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "No");
                }
                _getResourcesReq.AddHeader("Content-Type", "application/octet-stream");
                _getResourcesReq.AddHeader("Csrf-Token", CsrfToken);
                _getResourcesReq.AddCookie("csrftoken", CsrfToken);
                _getResourcesReq.AddCookie("asp.net_sessionid", SessionID);
                _getResourcesReq.AddCookie("CtxsAuthId", AuthID);
                log.Info("getIcaString, sessionID:" + SessionID + " authID:" + AuthID + " csrfToken:" + CsrfToken + " appID:" + AppID);
                IRestResponse _resourceListResp = _rc.Execute(_getResourcesReq);

                string _icaFileString = _resourceListResp.Content;
                log.Info("getIcaString, _icaFileString:" + _icaFileString);
                return _icaFileString;
            }
            catch(Exception ex)
            {
                log.Info("StorefrontController/getIcaString", ex.InnerException);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SessionID"></param>
        /// <param name="AuthID"></param>
        /// <param name="CsrfToken"></param>
        /// <param name="AppID"></param>
        /// <param name="UseSSL"></param>
        /// <returns></returns>
        public static string GetIcaStringAsJSON(string SessionID, string AuthID, string CsrfToken, string AppID, bool UseSSL)
        {
            try
            {
                string SFURL = null;

                if (UseSSL)
                {
                    SFURL = "https://" + baseSFURL;
                }
                else
                {
                    SFURL = "http://" + baseSFURL;
                }
                log.Info("SFURL:" + SFURL);
                RestClient _rc = new RestClient(SFURL);
                //RestRequest _getResourcesReq = new RestRequest(string.Format("Resources/LaunchIca/{0}", AppID), Method.GET);

                RestRequest _getResourcesReq = new RestRequest(AppID, Method.GET);

                if (UseSSL)
                {
                    _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "Yes");
                }
                else
                {
                    _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "No");
                }
                _getResourcesReq.AddHeader("Content-Type", "application/json");
                _getResourcesReq.AddHeader("Csrf-Token", CsrfToken);
                _getResourcesReq.AddCookie("csrftoken", CsrfToken);
                _getResourcesReq.AddCookie("asp.net_sessionid", SessionID);
                _getResourcesReq.AddCookie("CtxsAuthId", AuthID);
                IRestResponse _resourceListResp = _rc.Execute(_getResourcesReq);

                string _icaFileString = _resourceListResp.Content; // .Replace("\r","");

                return _icaFileString;// .Replace("\n","");
            }
            catch (Exception ex)
            {
                log.Info("StorefrontController/GetIcaStringAsJSON", ex);
                return null;
            }
        }

        [HttpGet]
        [Route("EC")]
        public IHttpActionResult EncryptLink()
        {
            var headers = Request.Headers;
            string link = "";
            try
            {
                if (headers.Contains("Encrypt"))
                {
                    link = headers.GetValues("Encrypt").First();
                }
            }
            catch (Exception ee)
            {
                return BadRequest();
            }

            string outlink = Crypto.Encrypt(link);

            return Ok(outlink);
        }
    }
}
