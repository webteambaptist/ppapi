﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Configuration;
using System.Net.Http;
using System.IO;
using PPAPI.Models;
using PPAPI.DowntimePlansSVC;
using PPAPI.OnCallSVC;
using PPAPI.Bylaws2016SVC;
using System.Net.Http.Headers;
using PPAPI.Helpers;
using System.Security.Permissions;
using System.Runtime.InteropServices;
using System.Runtime.ConstrainedExecution;
using Microsoft.Win32.SafeHandles;
using System.Security;
using System.Text;
using System.Security.Principal;

namespace PPAPI.Controllers
{
    [RoutePrefix("api/files")]
    public class OnCallSVCController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string OnCallUrl = ConfigurationManager.AppSettings["OnCall"].ToString();

        #region OnCallAPI
        /// <summary>
        /// Get EROnCallSchedulesItemFiles that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("EROnCallSchedulesItemFiles")]
        public IHttpActionResult retrieveEROnCallSchedulesItemFiles()
        {
            List<OnCallSVC.EROncallSchedulesItem> docs = new List<OnCallSVC.EROncallSchedulesItem>();

            try
            {
                //ClientContext ctx = new ClientContext("https://qaportal.bmcjax.com/Teams/QaPp");
                //http://spphysicians2010.e-baptisthealth.com/schedules/_vti_bin/listdata.svc

                SchedulesDataContext pp = new SchedulesDataContext(new Uri(OnCallUrl));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var a in pp.EROncallSchedules)
                {
                    docs.Add(a);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/EROnCallSchedulesItemFiles", ex);
                return BadRequest();
            }

            return Ok(docs);
        }

        /// <summary>
        /// Post EROnCallSchedulesItemFiles PDF based on the ID
        /// </summary>
        [HttpPost]
        [Route("EROnCallSchedulesItemFiles/{id}")]
        public IHttpActionResult retrieveEROnCallSchedulesItemFilesPDFByID(int id)
        {
            try
            {
                var headers = Request.Headers;
                int linkId = id;

                SchedulesDataContext pp = new SchedulesDataContext(new Uri(OnCallUrl));
                pp.Credentials = CredentialCache.DefaultCredentials;

                List<OnCallSVC.EROncallSchedulesItem> documentList = new List<OnCallSVC.EROncallSchedulesItem>();
                documentList = pp.EROncallSchedules.ToList();
                var doc = (from x in documentList where x.Id == linkId select x).First();

                System.Data.Services.Client.DataServiceStreamResponse DataServiceStreamResponse = pp.GetReadStream(doc);
                Stream stream = DataServiceStreamResponse.Stream;

                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(stream)
                };

                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = doc.Name
                };

                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

                var response = ResponseMessage(result);

                return response;
            }
            catch(Exception ex)
            {
                log.Error(ex);
                return BadRequest();
            }
        }

        /// <summary>
        /// Get EROncallSchedulesFacilityValueFiles that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("EROncallSchedulesFacilityValueFiles")]
        public IHttpActionResult retrieveEROncallSchedulesFacilityValueFiles()
        {
            List<OnCallSVC.EROncallSchedulesFacilityValue> docs = new List<OnCallSVC.EROncallSchedulesFacilityValue>();

            try
            {
                //ClientContext ctx = new ClientContext("https://qaportal.bmcjax.com/Teams/QaPp");
                //http://spphysicians2010.e-baptisthealth.com/schedules/_vti_bin/listdata.svc

                SchedulesDataContext pp = new SchedulesDataContext(new Uri(OnCallUrl));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var a in pp.EROncallSchedulesFacility)
                {
                    docs.Add(a);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/EROncallSchedulesFacilityValueFiles", ex);
                return BadRequest();
            }

            return Ok(docs);
        }
        #endregion

    }
}