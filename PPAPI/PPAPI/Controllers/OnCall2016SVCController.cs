﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Configuration;
using System.Net.Http;
using System.IO;
using PPAPI.Models;
using PPAPI.DowntimePlansSVC;
using PPAPI.OnCallSVC;
using PPAPI.Bylaws2016SVC;
using System.Net.Http.Headers;
using PPAPI.Helpers;
using System.Security.Permissions;
using System.Runtime.InteropServices;
using System.Runtime.ConstrainedExecution;
using Microsoft.Win32.SafeHandles;
using System.Security;
using System.Text;
using System.Security.Principal;

namespace PPAPI.Controllers
{
    [RoutePrefix("api/files")]
    public class OnCall2016SVCController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string OnCall2016Url = ConfigurationManager.AppSettings["OnCall2016"].ToString();

        #region OnCall2016API
        /// <summary>
        /// Get EROnCall2016SchedulesItemFiles that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("EROnCall2016SchedulesItemFiles")]
        public IHttpActionResult retrieveEROnCall2016SchedulesItemFiles()
        {
            List<OnCall2016SVC.EROncallScheduleItem> docs = new List<OnCall2016SVC.EROncallScheduleItem>();
            try
            {
                OnCall2016SVC.MedicalStaffOfficeDataContext pp = new OnCall2016SVC.MedicalStaffOfficeDataContext(new Uri(OnCall2016Url));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var a in pp.EROncallSchedule.Expand("Facility"))
                {
                    docs.Add(a);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/EROnCall2016SchedulesItemFiles", ex);
                return BadRequest();
            }
            return Ok(docs);
        }

        /// <summary>
        /// Get EROnCall2016SchedulesItemFilesBySpecialtyValue based on the SpecialtyValue string passed via Headers.
        /// </summary>
        [HttpGet]
        [Route("EROnCall2016SchedulesBySpecialtyValue")]
        public IHttpActionResult retrieveEROnCall2016SchedulesItemFilesBySpecialtyValue()
        {
            var headers = Request.Headers;
            string SpecialtyValue;

            List<OnCall2016SVC.EROncallScheduleItem> documentList = new List<OnCall2016SVC.EROncallScheduleItem>();
            List<OnCall2016SVC.EROncallScheduleItem> docsBySpecialty = new List<OnCall2016SVC.EROncallScheduleItem>();
            try
            {
                OnCall2016SVC.MedicalStaffOfficeDataContext pp = new OnCall2016SVC.MedicalStaffOfficeDataContext(new Uri(OnCall2016Url));
                pp.Credentials = CredentialCache.DefaultCredentials;
                documentList = pp.EROncallSchedule.ToList();

                if (headers.Contains("SpecialtyValue"))
                {
                    SpecialtyValue = headers.GetValues("SpecialtyValue").First();
                    docsBySpecialty = (from x in documentList where x.SpecialtyValue == SpecialtyValue select x).ToList();
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/EROnCall2016SchedulesItemFilesBySpecialtyValue", ex);
                return BadRequest();
            }
            return Ok(docsBySpecialty);
        }
        /// <summary>
        /// Get EROnCall2016SchedulesItemFilesByFacility based on the Facility string passed.
        /// </summary>
        [HttpGet]
        [Route("EROnCall2016SchedulesItemFilesByFacility")]
        public IHttpActionResult retrieveEROnCall2016SchedulesItemFilesByFacility()
        {
            var headers = Request.Headers;
            OnCall2016SVC.EROncallScheduleFacilityValue facility = new OnCall2016SVC.EROncallScheduleFacilityValue();
            facility.Value = headers.GetValues("Facility").First();
            List<OnCall2016SVC.EROncallScheduleItem> documentList = new List<OnCall2016SVC.EROncallScheduleItem>();
            List<OnCall2016SVC.EROncallScheduleItem> docsByFacility = new List<OnCall2016SVC.EROncallScheduleItem>();
            try
            {
                OnCall2016SVC.MedicalStaffOfficeDataContext pp = new OnCall2016SVC.MedicalStaffOfficeDataContext(new Uri(OnCall2016Url));
                pp.Credentials = CredentialCache.DefaultCredentials;
                documentList = pp.EROncallSchedule.Expand("Facility").ToList();

                foreach (var facilityArray in documentList)
                {
                    foreach(var value in facilityArray.Facility)
                    {
                        if(value.Value == facility.Value)
                        {
                            docsByFacility.Add(facilityArray);
                        }                        
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/EROnCall2016SchedulesItemFilesByFacility", ex);
                return BadRequest();
            }
            return Ok(docsByFacility);
        }

        /// <summary>
        /// Post EROnCall2016SchedulesItemFiles PDF based on the ID through Headers
        /// </summary>
        [HttpPost]
        [Route("EROnCall2016SchedulesItemFilesPDFByID")]
        public IHttpActionResult retrieveEROnCall2016SchedulesItemFilesPDFByID()
        {
            try
            {
                OnCall2016SVC.MedicalStaffOfficeDataContext pp = new OnCall2016SVC.MedicalStaffOfficeDataContext(new Uri(OnCall2016Url));
                pp.Credentials = CredentialCache.DefaultCredentials;
                OnCall2016SVC.EROncallScheduleItem doc = new OnCall2016SVC.EROncallScheduleItem();
                List<OnCall2016SVC.EROncallScheduleItem> documentList = new List<OnCall2016SVC.EROncallScheduleItem>();
                try
                {
                    var headers = Request.Headers;
                    string linkId;
                    documentList = pp.EROncallSchedule.ToList();

                    if (headers.Contains("ID"))
                    {
                        linkId = headers.GetValues("ID").First();
                        doc = (from x in documentList where x.Id.ToString() == linkId select x).First();
                    }
                }
                catch (Exception ex)
                {
                    log.Info("api/files/EROnCall2016SchedulesItemFilesPDFByID", ex);
                    return BadRequest();
                }

                System.Data.Services.Client.DataServiceStreamResponse DataServiceStreamResponse = pp.GetReadStream(doc);
                Stream stream = DataServiceStreamResponse.Stream;

                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(stream)
                };

                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = doc.Name
                };

                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                var response = ResponseMessage(result);
                return response;

            }
            catch (Exception ex)
            {
                log.Info("api/files/EROnCall2016SchedulesItemFilesPDFByID", ex);
                return BadRequest();
            }
        }

        /// <summary>
        /// Get EROncall2016SchedulesFacilityValueFiles that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("EROncall2016SchedulesFacilityValueFiles")]
        public IHttpActionResult retrieveEROncall2016SchedulesFacilityValueFiles()
        {
            List<OnCall2016SVC.EROncallScheduleFacilityValue> docs = new List<OnCall2016SVC.EROncallScheduleFacilityValue>();

            try
            {
                OnCall2016SVC.MedicalStaffOfficeDataContext pp = new OnCall2016SVC.MedicalStaffOfficeDataContext(new Uri(OnCall2016Url));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var a in pp.EROncallScheduleFacility)
                {
                    docs.Add(a);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/EROncall2016SchedulesFacilityValueFiles", ex);
                return BadRequest();
            }
            return Ok(docs);
        }
        /// <summary>
        /// Get EROncall2016ScheduleSpecialtyValue that are available to retrieve.
        /// </summary>
        [HttpGet]
        [Route("EROncall2016ScheduleSpecialtyValue")]
        public IHttpActionResult retrieveEROncall2016ScheduleSpecialtyValueFiles()
        {
            List<OnCall2016SVC.EROncallScheduleSpecialtyValue> docs = new List<OnCall2016SVC.EROncallScheduleSpecialtyValue>();

            try
            {
                OnCall2016SVC.MedicalStaffOfficeDataContext pp = new OnCall2016SVC.MedicalStaffOfficeDataContext(new Uri(OnCall2016Url));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach (var a in pp.EROncallScheduleSpecialty)
                {
                    docs.Add(a);
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/EROncall2016ScheduleSpecialtyValue", ex);
                return BadRequest();
            }
            return Ok(docs);
        }

        /// <summary>
        /// Gets an EROnCallSchedule Object (2016) list that includes Facility, Specialty, Title and ID.
        /// </summary>
        [HttpGet]
        [Route("EROnCall2016ScheduleList")]
        public IHttpActionResult retrieveEROncall2016ScheduleList()
        {
            List<OnCall2016SVC.EROncallScheduleSpecialtyValue> docs = new List<OnCall2016SVC.EROncallScheduleSpecialtyValue>();
            FacilityList FacilityList = new FacilityList();
            FacilityList.Facilities = new List<Facility>();

            try
            {
                OnCall2016SVC.MedicalStaffOfficeDataContext pp = new OnCall2016SVC.MedicalStaffOfficeDataContext(new Uri(OnCall2016Url));
                pp.Credentials = CredentialCache.DefaultCredentials;

                foreach(var schedule in pp.EROncallSchedule.Expand("Facility"))
                {
                    EROnCallSchedule EROnCallSchedule = new EROnCallSchedule();
                    EROnCallSchedule.ID = schedule.Id;
                    EROnCallSchedule.Title = schedule.Name;
                    EROnCallSchedule.Specialty = schedule.SpecialtyValue;

                    foreach(var facilityName in schedule.Facility)
                    {
                        Facility Facility = new Facility();
                        Facility.ErOnCallSchedule = new List<EROnCallSchedule>();
                        Facility.FacilityName = facilityName.Value;
                        Facility.ErOnCallSchedule.Add(EROnCallSchedule);

                        FacilityList.Facilities.Add(Facility);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files/EROnCall2016ScheduleList", ex);
                return BadRequest();
            }

            return Ok(FacilityList);
        }

        #endregion
    }
}