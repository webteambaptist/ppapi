﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PPAPI.Models;

namespace PPAPI.Controllers
{
    [RoutePrefix("api/MedicalLibrary")]
    public class MedicalLibraryController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        [Route("getoptions")]
        [HttpGet]
        public IHttpActionResult getOptions()
        {
            //var headers = Request.Headers;
            //string echoID = "";

            try
            {
                var ppEntities = new PhysiciansPortalEntities();
                var medicalLibraryOptions =  ppEntities.MedicalLibraryOptions.ToList();

                var mloList = new List<MedicalLibraryOptions>();
                
                foreach (var option in medicalLibraryOptions)
                {

                    var optionValues = ppEntities.MedicalLibraryOptionValues.Where(x => x.OptionsId == option.ID)
                        .ToList();

                    foreach (var value in optionValues)
                    {
                        var mlov = new MedicalLibraryOptions()
                        {
                            OptionName = option.DropDownName,
                            OptionValue = value.Value
                        };
                        mloList.Add(mlov); 
                    }
                }
                return Json(mloList);               
            }
            catch (Exception ex)
            {
                log.Info("api/MedicalLibrary/getoptions", ex);
                return BadRequest();
            }
        }
    }
}
