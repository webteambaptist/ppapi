﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPAPI.Models
{
    public class MedicalLibraryOptions
    {
        public string OptionName { get; set; }
        public string OptionValue { get; set; }
    }
}