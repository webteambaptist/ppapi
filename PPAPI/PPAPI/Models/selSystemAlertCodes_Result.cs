//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PPAPI.Models
{
    using System;
    
    public partial class selSystemAlertCodes_Result
    {
        public int id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string IconColor { get; set; }
        public string ForeColor { get; set; }
        public string BackColor { get; set; }
        public Nullable<bool> UseTicker { get; set; }
        public Nullable<bool> UseDialog { get; set; }
        public Nullable<bool> UseOSBubble { get; set; }
        public Nullable<bool> UseTrayToast { get; set; }
        public Nullable<System.DateTime> lup_dt { get; set; }
        public string lup_user { get; set; }
        public Nullable<bool> IsArchived { get; set; }
        public Nullable<bool> ShowPC { get; set; }
        public Nullable<bool> ShowBHTHIN { get; set; }
        public Nullable<bool> ShowNetScaler { get; set; }
        public Nullable<bool> ShowPhysiciansPortal { get; set; }
        public Nullable<bool> ShowIntranet { get; set; }
        public Nullable<bool> ShowPFCS { get; set; }
    }
}
