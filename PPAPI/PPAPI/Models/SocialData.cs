﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPAPI.Models
{
    public class SocialData
    {
        // public SocialFeedMain socialFeed { get; set; }
        public int ID { get; set; }
        public int PostID { get; set; }
        public string PostAuthor { get; set; }
        public string PostDate { get; set; }
        public string PostBody { get; set; }
        public string CreatedDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string ECHOID { get; set; }
        public List<SocialFeedAttachment> attachments { get; set; }
        public string PostAuthorEchoID { get; set; }

    }

    public class Subs
    {
        public int ID { get; set; }
        public string SubscriptionPhysicianID { get; set; }
        public string SubscribersPhysicianID { get; set; }
        public string SubscriptionPhysicianName { get; set; }
        public string SubscribersPhysicianName { get; set; }
    }

    public class SubsPubs
    {
        public bool Subscribed { get; set; }
        public List<Subs> Subscribers { get; set; }
        public List<Subs> Subscriptions { get; set; }
    }
}