﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PPAPI.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class PhysiciansPortal_QAEntities : DbContext
    {
        public PhysiciansPortal_QAEntities()
            : base("name=PhysiciansPortalEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Fact_FormEvents> Fact_FormEvents { get; set; }
        public virtual DbSet<Fact_FormStatisticsByContact> Fact_FormStatisticsByContact { get; set; }
        public virtual DbSet<Fact_FormSummary> Fact_FormSummary { get; set; }
        public virtual DbSet<FormFieldValue> FormFieldValues { get; set; }
        public virtual DbSet<Hospital> Hospitals { get; set; }
        public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<PhysicianQuicklink> PhysicianQuicklinks { get; set; }
        public virtual DbSet<PhysicianTaskList> PhysicianTaskLists { get; set; }
        public virtual DbSet<SocialFeedAttachment> SocialFeedAttachments { get; set; }
        public virtual DbSet<SocialFeedMain> SocialFeedMains { get; set; }
        public virtual DbSet<AppImageMatrix> AppImageMatrices { get; set; }
        public virtual DbSet<GeneralPhysicianInfo> GeneralPhysicianInfoes { get; set; }
        public virtual DbSet<PhysicianInfoHub> PhysicianInfoHubs { get; set; }
        public virtual DbSet<PortalLog> PortalLogs { get; set; }
        public virtual DbSet<PPDailyCheck> PPDailyChecks { get; set; }
        public virtual DbSet<ScheduleHtml> ScheduleHtmls { get; set; }
        public virtual DbSet<SocialFeedMatrix> SocialFeedMatrices { get; set; }
        public virtual DbSet<MedicalLibraryOptionValue> MedicalLibraryOptionValues { get; set; }
        public virtual DbSet<MedicalLibraryOption> MedicalLibraryOptions { get; set; }
    
        public virtual int Add_FormStatisticsByContact(Nullable<System.Guid> contactId, Nullable<System.Guid> formId, Nullable<System.DateTime> lastInteractionDate, Nullable<int> submits, Nullable<int> success, Nullable<int> dropouts, Nullable<int> failures, Nullable<int> visits, Nullable<int> value, Nullable<int> finalResult)
        {
            var contactIdParameter = contactId.HasValue ?
                new ObjectParameter("ContactId", contactId) :
                new ObjectParameter("ContactId", typeof(System.Guid));
    
            var formIdParameter = formId.HasValue ?
                new ObjectParameter("FormId", formId) :
                new ObjectParameter("FormId", typeof(System.Guid));
    
            var lastInteractionDateParameter = lastInteractionDate.HasValue ?
                new ObjectParameter("LastInteractionDate", lastInteractionDate) :
                new ObjectParameter("LastInteractionDate", typeof(System.DateTime));
    
            var submitsParameter = submits.HasValue ?
                new ObjectParameter("Submits", submits) :
                new ObjectParameter("Submits", typeof(int));
    
            var successParameter = success.HasValue ?
                new ObjectParameter("Success", success) :
                new ObjectParameter("Success", typeof(int));
    
            var dropoutsParameter = dropouts.HasValue ?
                new ObjectParameter("Dropouts", dropouts) :
                new ObjectParameter("Dropouts", typeof(int));
    
            var failuresParameter = failures.HasValue ?
                new ObjectParameter("Failures", failures) :
                new ObjectParameter("Failures", typeof(int));
    
            var visitsParameter = visits.HasValue ?
                new ObjectParameter("Visits", visits) :
                new ObjectParameter("Visits", typeof(int));
    
            var valueParameter = value.HasValue ?
                new ObjectParameter("Value", value) :
                new ObjectParameter("Value", typeof(int));
    
            var finalResultParameter = finalResult.HasValue ?
                new ObjectParameter("FinalResult", finalResult) :
                new ObjectParameter("FinalResult", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Add_FormStatisticsByContact", contactIdParameter, formIdParameter, lastInteractionDateParameter, submitsParameter, successParameter, dropoutsParameter, failuresParameter, visitsParameter, valueParameter, finalResultParameter);
        }
    
        public virtual int delAttach(Nullable<int> postID)
        {
            var postIDParameter = postID.HasValue ?
                new ObjectParameter("PostID", postID) :
                new ObjectParameter("PostID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("delAttach", postIDParameter);
        }
    
        public virtual int delMatrix(string subscriptionPhysicianID, string subscribersPhysicianID)
        {
            var subscriptionPhysicianIDParameter = subscriptionPhysicianID != null ?
                new ObjectParameter("SubscriptionPhysicianID", subscriptionPhysicianID) :
                new ObjectParameter("SubscriptionPhysicianID", typeof(string));
    
            var subscribersPhysicianIDParameter = subscribersPhysicianID != null ?
                new ObjectParameter("SubscribersPhysicianID", subscribersPhysicianID) :
                new ObjectParameter("SubscribersPhysicianID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("delMatrix", subscriptionPhysicianIDParameter, subscribersPhysicianIDParameter);
        }
    
        public virtual int delPost(Nullable<int> postID)
        {
            var postIDParameter = postID.HasValue ?
                new ObjectParameter("PostID", postID) :
                new ObjectParameter("PostID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("delPost", postIDParameter);
        }
    
        public virtual int delQuickLinks(string echoid)
        {
            var echoidParameter = echoid != null ?
                new ObjectParameter("echoid", echoid) :
                new ObjectParameter("echoid", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("delQuickLinks", echoidParameter);
        }
    
        public virtual int insInfo(string echoid, string contentid, Nullable<System.DateTime> readDate)
        {
            var echoidParameter = echoid != null ?
                new ObjectParameter("echoid", echoid) :
                new ObjectParameter("echoid", typeof(string));
    
            var contentidParameter = contentid != null ?
                new ObjectParameter("contentid", contentid) :
                new ObjectParameter("contentid", typeof(string));
    
            var readDateParameter = readDate.HasValue ?
                new ObjectParameter("ReadDate", readDate) :
                new ObjectParameter("ReadDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("insInfo", echoidParameter, contentidParameter, readDateParameter);
        }
    
        public virtual int insInfo2(string echoid, string contentid, Nullable<System.DateTime> readDate)
        {
            var echoidParameter = echoid != null ?
                new ObjectParameter("echoid", echoid) :
                new ObjectParameter("echoid", typeof(string));
    
            var contentidParameter = contentid != null ?
                new ObjectParameter("contentid", contentid) :
                new ObjectParameter("contentid", typeof(string));
    
            var readDateParameter = readDate.HasValue ?
                new ObjectParameter("ReadDate", readDate) :
                new ObjectParameter("ReadDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("insInfo2", echoidParameter, contentidParameter, readDateParameter);
        }
    
        public virtual int insSubs(string pub, string sub)
        {
            var pubParameter = pub != null ?
                new ObjectParameter("pub", pub) :
                new ObjectParameter("pub", typeof(string));
    
            var subParameter = sub != null ?
                new ObjectParameter("sub", sub) :
                new ObjectParameter("sub", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("insSubs", pubParameter, subParameter);
        }
    
        public virtual ObjectResult<selSocialFeed_Result> selSocialFeed(Nullable<int> postID)
        {
            var postIDParameter = postID.HasValue ?
                new ObjectParameter("PostID", postID) :
                new ObjectParameter("PostID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<selSocialFeed_Result>("selSocialFeed", postIDParameter);
        }
    
        public virtual ObjectResult<selSocialFeed2_Result> selSocialFeed2(Nullable<int> postID)
        {
            var postIDParameter = postID.HasValue ?
                new ObjectParameter("PostID", postID) :
                new ObjectParameter("PostID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<selSocialFeed2_Result>("selSocialFeed2", postIDParameter);
        }
    
        public virtual int updInfo(Nullable<int> iD, Nullable<System.DateTime> rDate)
        {
            var iDParameter = iD.HasValue ?
                new ObjectParameter("ID", iD) :
                new ObjectParameter("ID", typeof(int));
    
            var rDateParameter = rDate.HasValue ?
                new ObjectParameter("RDate", rDate) :
                new ObjectParameter("RDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("updInfo", iDParameter, rDateParameter);
        }
    
        public virtual int updSocialFeed(Nullable<int> postID, string postBody)
        {
            var postIDParameter = postID.HasValue ?
                new ObjectParameter("PostID", postID) :
                new ObjectParameter("PostID", typeof(int));
    
            var postBodyParameter = postBody != null ?
                new ObjectParameter("PostBody", postBody) :
                new ObjectParameter("PostBody", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("updSocialFeed", postIDParameter, postBodyParameter);
        }
    
        public virtual int updTask(Nullable<int> iD, Nullable<System.DateTime> sDate)
        {
            var iDParameter = iD.HasValue ?
                new ObjectParameter("ID", iD) :
                new ObjectParameter("ID", typeof(int));
    
            var sDateParameter = sDate.HasValue ?
                new ObjectParameter("SDate", sDate) :
                new ObjectParameter("SDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("updTask", iDParameter, sDateParameter);
        }
    }
}
