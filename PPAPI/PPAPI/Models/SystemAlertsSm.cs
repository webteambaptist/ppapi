﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPAPI.Models
{
    public class SystemAlertsSm
    {
        public int id { get; set; }
        public string Message { get; set; }
        public bool IsPhysicianPortalVisible { get; set; }
        public DateTime? StartDt { get; set; }
        public DateTime? EndDt { get; set; }
        public bool IsEverywhere { get; set; }
     //   public bool IsEnabled { get; set; }
    }
}