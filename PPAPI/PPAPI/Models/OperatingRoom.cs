﻿using PPAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhysicianPortal.Models
{
    public class OperatingRoom
    {
        public List<ORSchedules> schedules { get; set; }
    }
}