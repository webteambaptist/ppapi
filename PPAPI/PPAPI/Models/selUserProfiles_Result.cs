//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PPAPI.Models
{
    using System;
    
    public partial class selUserProfiles_Result
    {
        public int id { get; set; }
        public string ntID { get; set; }
        public string userType { get; set; }
        public string userTitle { get; set; }
        public string userDept { get; set; }
        public Nullable<System.DateTime> lastLogin { get; set; }
        public Nullable<System.DateTime> lastUpdate { get; set; }
        public Nullable<int> RoleCt { get; set; }
    }
}
